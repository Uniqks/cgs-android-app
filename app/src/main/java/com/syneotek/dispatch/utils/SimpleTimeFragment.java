package com.syneotek.dispatch.utils;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import com.syneotek.dispatch.Fragments.ManageJob;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Syneotek on 20/05/2016.
 */
public class SimpleTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    TimePickerDialog timePickerDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int hr = calendar.get(Calendar.HOUR_OF_DAY);
        int mm = calendar.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(getActivity(), this, hr, mm, true);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int which) {
                        timePickerDialog.cancel();
                    }
                });
        timePickerDialog.setCancelable(false);
        timePickerDialog.setCanceledOnTouchOutside(false);
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        populateSetDate(hourOfDay, minute);
    }

    public void populateSetDate(int hr, int min) {
        ManageJob.txt_from_time.setText(parseDateToddMMyyyy(hr + ":" + min));
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "hh:mm";
        String outputPattern = "HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}