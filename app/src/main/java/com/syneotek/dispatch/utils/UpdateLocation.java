package com.syneotek.dispatch.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.dispatch.survey_details.GPSTracker;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Syneotek on 01/04/2016.
 */
public class UpdateLocation extends Service {

    GPSTracker gps;
    private static final String url = Const.HOSTNAME + "wslocation.php?";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gps = new GPSTracker(UpdateLocation.this);

        Log.d("Service", "Created");


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
                if (gps.canGetLocation()) {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    Log.d("lat lng", latitude + " " + longitude);
                    sendLocation(String.valueOf(latitude), String.valueOf(longitude));
                }

                handler.postDelayed(this, 20000);
            }
        }, 20000);








       /* if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            sendLocation(String.valueOf(latitude), String.valueOf(longitude));
        } else {
//            gps.showSettingsAlert();
        }*/
    }

    void sendLocation(final String lat, final String lang) {
        SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
        String uid = sharedpreferences.getString("uid", "0");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        if (isNetworkAvailable()) {
            final StringRequest jsonObjMyLoc = new StringRequest(Request.Method.GET, url + "lat=" + lat + "&lang=" + lang + "&driver_id=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.e("LocationUpdate", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonstatus = object.getString("status");

                        switch (Integer.parseInt(jsonstatus)) {
                            case 1:
//                                Toast.makeText(getApplicationContext(), "Lat :"+lat+"\n Lang :"+lang, Toast.LENGTH_LONG).show();
                                Log.e("Location : ", "Lat :" + lat + "\n Lang :" + lang);
                                break;
                            default:
//                                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getApplicationContext(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Loc Err :", "Err :" + error.toString());
//                    Toast.makeText(getApplicationContext(), "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(jsonObjMyLoc);
        } else {
//            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager my_loc_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = my_loc_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
