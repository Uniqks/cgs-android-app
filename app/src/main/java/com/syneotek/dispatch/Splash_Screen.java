package com.syneotek.dispatch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.text.SimpleDateFormat;
import java.util.Date;

import rx.functions.Action1;

public class Splash_Screen extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    public static String cur_date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spalsh_screen);

        CheckPermissions();
        /*new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(SplashScreen.this, Splash_Screen.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);*/
    }

    public void initialize(boolean isAppInitialized) {
        if (isAppInitialized) {
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            editor = sharedpreferences.edit();
            final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            cur_date = df.format(new Date());
            editor.putString("cur_date", cur_date);
            editor.commit();
            Thread background = new Thread() {
                public void run() {
                    try {
                        sleep(2 * 1500);
//                        Intent i = new Intent(getBaseContext(), App_Logo_Activity.class);
//                        startActivity(i);
//                        finish();
                        Intent intent;
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        if (sharedpreferences.contains("uid")) {
                            intent = new Intent(Splash_Screen.this, Main_Activity_Nevigation.class);
                        } else {
                            intent = new Intent(Splash_Screen.this, Login_Activity.class);
                        }
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            // start thread
            background.start();

        } else {
            /* If one Of above permission not grant show alert (force to grant permission)*/
            AlertDialog.Builder builder = new AlertDialog.Builder(Splash_Screen.this);
            builder.setTitle("Alert");
            builder.setMessage("All permissions necessary");
            builder.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CheckPermissions();
                }
            });
            builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.show();
        }
    }

    void CheckPermissions() {
        /*android.Manifest.permission.WRITE_EXTERNAL_STORAGE,*/
        RxPermissions.getInstance(Splash_Screen.this)
                .request(android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
//                        android.Manifest.permission.CALL_PHONE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.CAMERA
//                        android.Manifest.permission.PROCESS_OUTGOING_CALLS
                )
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        initialize(aBoolean);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
