package com.syneotek.dispatch;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.syneotek.dispatch.Fragments.App_Name_Frag1;
import com.syneotek.dispatch.Fragments.Calender_Frag;
import com.syneotek.dispatch.Fragments.ExtraRequested;
import com.syneotek.dispatch.Fragments.Google_map_Frag;
import com.syneotek.dispatch.Fragments.Help_Fragment;
import com.syneotek.dispatch.Fragments.ManageJob;
import com.syneotek.dispatch.Fragments.ManageQuotation;
import com.syneotek.dispatch.Fragments.RatingReviewFragment;
import com.syneotek.dispatch.Fragments.Setting_Frag;
import com.syneotek.dispatch.Fragments.ViewGalleryImages;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.utils.Const;

public class Main_Activity_Nevigation extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private DrawerLayout mDrawerLayout;
    FragmentManager fm_map;
    FragmentTransaction fragmentTransaction_map;
    TextView txt_title;
    ImageView img_recycle;
    private GoogleApiClient googleApiClient;
    public static boolean isMessage = false;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    private int screenWidth, screenHeight;
    DisplayMetrics display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_nevigation);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        Const.verifyMapPermissions(Main_Activity_Nevigation.this);
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(Main_Activity_Nevigation.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
//            **************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        Main_Activity_Nevigation.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

        Intent alarm = new Intent(this, LocationBroadcastReceiver.class);
        boolean alarmRunning = (PendingIntent.getBroadcast(this, 0, alarm, PendingIntent.FLAG_NO_CREATE) != null);
        if (alarmRunning == false) {
            Log.e("Start Service", "Service started");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarm, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1 * 60000, pendingIntent);
        }

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        img_recycle = (ImageView) findViewById(R.id.img_recycle);
        img_recycle.setImageResource(R.mipmap.ic_syn);

        isMessage = Boolean.parseBoolean(sharedpreferences.getString("IsMessageGCM", "false"));
        Log.e("IsMessage", "" + isMessage);
        fm_map = getSupportFragmentManager();
        if (isMessage) {
            isMessage = false;
            editor = sharedpreferences.edit();
            editor.putString("IsMessageGCM", String.valueOf(isMessage));
            editor.apply();
            txt_title.setText("Help");
            img_recycle.setVisibility(View.INVISIBLE);
            fragmentTransaction_map = fm_map.beginTransaction();
            fragmentTransaction_map.replace(R.id.main_screen_framelayout, new Help_Fragment()).commit();
        } else {
            isMessage = false;
            txt_title.setText("CGS");
            fragmentTransaction_map = fm_map.beginTransaction();
            fragmentTransaction_map.replace(R.id.main_screen_framelayout, new App_Name_Frag1()).commit();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_screen_drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.main_screen_navigation_view);
        View header = navigationView.getHeaderView(0);
        for (int i = 0; i < navigationView.getChildCount(); i++) {
            ((NavigationMenuView) navigationView.getChildAt(i)).setPadding(0, -5, 0, -5);
        }
        navigationView.setItemIconTintList(null);
        setLayout(header, 30, 80);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(false);
                mDrawerLayout.closeDrawers();
                // FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                switch (menuItem.getItemId()) {

                    case R.id.nav_home:
                        txt_title.setText("CGS");
                        img_recycle.setVisibility(View.VISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new App_Name_Frag1()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_calender:
                        txt_title.setText("Calender");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new Calender_Frag()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_gallery:
                        txt_title.setText("Gallery");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new ViewGalleryImages()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_manage_job:
                        txt_title.setText("Add New Job");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new ManageJob()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_manage_quotation:
                        txt_title.setText("Manage Quotation");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new ManageQuotation()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_help:
                        txt_title.setText("Help");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new Help_Fragment()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_map:
                        txt_title.setText("Map");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new Google_map_Frag()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_setting:
                        txt_title.setText("Setting");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new Setting_Frag()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_rating_review:
                        txt_title.setText("Rating & Review");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new RatingReviewFragment()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_extra:
                        txt_title.setText("Extra Requested");
                        img_recycle.setVisibility(View.INVISIBLE);
                        fragmentTransaction_map = fm_map.beginTransaction();
                        fragmentTransaction_map.replace(R.id.main_screen_framelayout, new ExtraRequested()).addToBackStack(null);
                        fragmentTransaction_map.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
                        fragmentTransaction_map.commit();
                        break;
                    case R.id.nav_logout:
                        logout();
//                        Intent intent = new Intent(Main_Activity_Nevigation.this, Login_Activity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
//                        overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                        finish();
                        break;
                }
                return false;
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_screen_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setHomeButtonEnabled(true);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(" ");
        actionBar.setHomeAsUpIndicator(R.drawable.image_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        img_recycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_title.setText("CGS");
                img_recycle.setVisibility(View.VISIBLE);
                fragmentTransaction_map = fm_map.beginTransaction();
                fragmentTransaction_map.replace(R.id.main_screen_framelayout, new App_Name_Frag1()).addToBackStack(null);
                fragmentTransaction_map.commit();
            }
        });
    }

    void setLayout(View view, int height, int width) {
        display = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(display);
        screenWidth = display.widthPixels;
        screenHeight = display.heightPixels;

        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = (screenHeight * height) / 100;
//        params.width = (screenWidth * width) / 100;
        view.setLayoutParams(params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //BACK HARDWARE BUTTON
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {
        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        moveTaskToBack(true);
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                }).show();
    }


    public void logout() {
        SharedPreferences sharedpreferences = getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        //getActivity().moveTaskToBack(true);
        Intent i = new Intent(getBaseContext(), Login_Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
        finish();
    }

    public void exit(View view) {
        //getActivity().moveTaskToBack(true);
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        finish();
    }
}
