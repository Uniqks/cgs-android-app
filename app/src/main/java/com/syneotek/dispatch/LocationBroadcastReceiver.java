package com.syneotek.dispatch;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.syneotek.dispatch.utils.UpdateLocation;

/**
 * Created by Syneotek on 01/04/2016.
 */
public class LocationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1=new Intent(context, UpdateLocation.class);
        context.startService(intent1);
    }
}
