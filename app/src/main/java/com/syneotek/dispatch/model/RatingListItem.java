package com.syneotek.dispatch.model;

public class RatingListItem {

    private String jobName;
    private String installationRating;
    private String cleanRating;
    private String comment;
    private String jobExplain;

    public String getJobExplain() {
        return jobExplain;
    }

    public void setJobExplain(String jobExplain) {
        this.jobExplain = jobExplain;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getInstallationRating() {
        return installationRating;
    }

    public void setInstallationRating(String installationRating) {
        this.installationRating = installationRating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCleanRating() {
        return cleanRating;
    }

    public void setCleanRating(String cleanRating) {
        this.cleanRating = cleanRating;
    }
}