package com.syneotek.dispatch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.syneotek.dispatch.R;
import com.syneotek.dispatch.model.HelpMsg;
import com.syneotek.dispatch.utils.Const;

import java.util.List;


public class Help_Msg_Adapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<HelpMsg> app_name_itemses;
    // ImageLoader imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();

    public Help_Msg_Adapter(Activity activity, List<HelpMsg> app_name_itemses) {
        this.activity = activity;
        this.app_name_itemses = app_name_itemses;
    }

    @Override
    public int getCount() {
        return app_name_itemses.size();
    }

    @Override
    public Object getItem(int location) {
        return app_name_itemses.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.help_list_item, null);

        //  if (imageLoader == null)
        //    imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();


        TextView text_msg_text = (TextView) convertView.findViewById(R.id.text_msg_text);
        TextView text_msg_date = (TextView) convertView.findViewById(R.id.text_msg_date);

        SharedPreferences sharedpreferences = activity.getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
        String uid = sharedpreferences.getString("uid", "0");

        HelpMsg m = new HelpMsg();
        m = app_name_itemses.get(position);
        text_msg_text.setText(m.help_message.replace("%20", " "));
        text_msg_date.setText(m.date);

        if (m.sender_type.equalsIgnoreCase("D")) {
            text_msg_text.setGravity(View.FOCUS_LEFT);
            text_msg_date.setGravity(View.FOCUS_LEFT);
            text_msg_text.setPadding(200, 9, 8, 9);
            text_msg_date.setPadding(15, 9, 8, 9);
            text_msg_text.setGravity(Gravity.RIGHT);
            text_msg_date.setGravity(Gravity.RIGHT);
            // text_msg_date.setPadding(View.FOCUS_LEFT,9,10,9);
//            convertView.setBackgroundColor(Color.YELLOW);
        } else if (!m.sender_type.equalsIgnoreCase("D")) {
            text_msg_text.setGravity(View.FOCUS_RIGHT);
            text_msg_date.setGravity(View.FOCUS_RIGHT);
            text_msg_text.setPadding(8, 9, 200, 9);
            text_msg_date.setPadding(8, 9, 15, 9);
            text_msg_text.setGravity(Gravity.LEFT);
            text_msg_date.setGravity(Gravity.LEFT);
//            convertView.setBackgroundColor(Color.RED);
        }
        return convertView;
    }

}