package com.syneotek.dispatch.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.syneotek.dispatch.Fragments.app_name_items;
import com.syneotek.dispatch.R;

import java.util.List;


public class CustomListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<app_name_items> app_name_itemses;
    // ImageLoader imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();

    public CustomListAdapter(Activity activity, List<app_name_items> app_name_itemses) {
        this.activity = activity;
        this.app_name_itemses = app_name_itemses;
    }

    @Override
    public int getCount() {
        return app_name_itemses.size();
    }

    @Override
    public Object getItem(int location) {
        return app_name_itemses.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_details, null);

        //  if (imageLoader == null)
        //    imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();


        TextView details = (TextView) convertView.findViewById(R.id.txt_name);
        TextView address = (TextView) convertView.findViewById(R.id.txt_address);
        TextView workingTime = (TextView) convertView.findViewById(R.id.txt_time);
        TextView workingDate = (TextView) convertView.findViewById(R.id.txt_date);
        TextView status = (TextView) convertView.findViewById(R.id.txt_status);
        LinearLayout mainLayout = (LinearLayout) convertView.findViewById(R.id.mainLayout);


        app_name_items m = app_name_itemses.get(position);

        details.setText(m.getDetails());
        address.setText(m.getAddress());
        workingDate.setText(m.getWorkingdate());
        workingTime.setText(m.getfrom_time());
        if (m.getStatus().equalsIgnoreCase("c")) {
            status.setText("Completed");
//            convertView.setBackgroundColor(Color.parseColor("#138808"));
            mainLayout.setBackgroundResource(R.drawable.completed);
        } else if (m.getStatus().equalsIgnoreCase("p")) {
            status.setText("Pending");
//            convertView.setBackgroundColor(Color.parseColor("#ffff00"));
            mainLayout.setBackgroundResource(R.drawable.pending);
        } else if (m.getStatus().equalsIgnoreCase("ne")) {
            status.setText("Not Excepted");
//            convertView.setBackgroundColor(Color.parseColor("#ff0000"));
            mainLayout.setBackgroundResource(R.drawable.not_excepted);
        } else if (m.getStatus().equalsIgnoreCase("e")) {
            status.setText("Accepted");
//            convertView.setBackgroundColor(Color.parseColor("#ffa500"));
            mainLayout.setBackgroundResource(R.drawable.excepted);
        } else if (m.getStatus().equalsIgnoreCase("a")) {
            status.setText("Created");
//            convertView.setBackgroundColor(Color.rgb(39, 120, 129));
            mainLayout.setBackgroundResource(R.drawable.created);
        }


        return convertView;
    }

}