package com.syneotek.dispatch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syneotek.dispatch.R;
import com.syneotek.dispatch.model.RatingListItem;

import java.util.List;


public class RatingListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<RatingListItem> listItems;
    // ImageLoader imageLoader = com.syneotek.agc.utils.App.getInstance().getImageLoader();

    public RatingListAdapter(Activity activity, List<RatingListItem> ratingListItems) {
        this.activity = activity;
        this.listItems = ratingListItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int location) {
        return listItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.rating_list_item, null);

        //  if (imageLoader == null)
        //    imageLoader = com.syneotek.agc.utils.App.getInstance().getImageLoader();


        TextView jobName = (TextView) convertView.findViewById(R.id.jobName);
        TextView jobRatingInstallation = (TextView) convertView.findViewById(R.id.jobRatingInstallation);
        TextView jobRatingClean = (TextView) convertView.findViewById(R.id.jobRatingClean);
        ImageView comment = (ImageView) convertView.findViewById(R.id.comment);
        TextView jobRatingExplain = (TextView) convertView.findViewById(R.id.jobRatingExplain);


        final RatingListItem m = listItems.get(position);

        jobName.setText(m.getJobName());
        jobRatingInstallation.setText("Installation complete = " + m.getInstallationRating());
        jobRatingClean.setText("Clean = " + m.getCleanRating());

        jobRatingExplain.setText("Explain : " + m.getJobExplain());


        if ((m.getComment().equals(""))) {
            comment.setVisibility(View.GONE);
        }
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(activity)
                        .setTitle("Comment")
                        .setMessage(m.getComment())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        return convertView;
    }

}