package com.syneotek.dispatch.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.syneotek.dispatch.R;

import java.util.ArrayList;

/**
 * Created by Synotek on 08/11/2016.
 */

public class MyPublicButtAdapter extends RecyclerView.Adapter<MyPublicButtAdapter.ViewHolder> {

    private ArrayList<String> mDataSet = new ArrayList<String>();
    Activity activity;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyPublicButtAdapter(Activity activity, ArrayList<String> myDataset) {
        this.mDataSet = myDataset;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String url = mDataSet.get(position);
//        holder.imageView.setText(mDataset.get(position));
        Log.e("MyPublicButtAdapter", "Position: " + position + " URL: " + url);

        Picasso.with(activity)
//                .load("http://richpersian.com/CGS/userPhoto_thumb/" + url)
                .load(url)
                .fit()
                .into(holder.imageView);

//        aQuery.id(holder.imageView).image(module.image, options)
//                .progress(R.id.progressProfile);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(holder.imageView.getDrawable(), url);
            }
        });

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
                intent.putExtra(Intent.EXTRA_TEXT, url);
                activity.startActivity(Intent.createChooser(intent, "Share image via..."));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, img_share;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            img_share = (ImageView) itemView.findViewById(R.id.img_share);
        }
    }

    private void openDialog(Drawable drawable, final String module) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.my_public_butt_dailog, null);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        final AlertDialog b = dialogBuilder.create();
        //noinspection ConstantConditions
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();

        ImageView btnClose = (ImageView) dialogView.findViewById(R.id.btnClose);
        SelectableRoundedImageView imageView = (SelectableRoundedImageView) dialogView.findViewById(R.id.imageView);
        ImageView img_share = (ImageView) dialogView.findViewById(R.id.img_share);

        Picasso.with(activity)
//                .load("http://richpersian.com/CGS/userPhoto_thumb/" + module)
                .load(module)
                .fit()
                .into(imageView);
//        imageView.setImageDrawable(drawable);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
                intent.putExtra(Intent.EXTRA_TEXT, module);
                activity.startActivity(Intent.createChooser(intent, "Share image via..."));
            }
        });
    }
}
