package com.syneotek.dispatch.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.syneotek.dispatch.R;
import com.syneotek.dispatch.model.Items_Spinner;

import java.util.List;


public class Spinner_Adapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<Items_Spinner> Items_Spinners;
    // ImageLoader imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();

    public Spinner_Adapter(Activity activity, List<Items_Spinner> items_spinners)
    {
        this.activity = activity;
        this.Items_Spinners = items_spinners;
    }

    @Override
    public int getCount()
    {
        return Items_Spinners.size();
    }

    @Override
    public Object getItem(int location)

    {
        return Items_Spinners.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent)
    {

        return getView(position, convertView, parent);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_item, null);

        //  if (imageLoader == null)
        //    imageLoader = com.syneotek.dispatch.utils.App.getInstance().getImageLoader();


        TextView details = (TextView) convertView.findViewById(R.id.txt_spinner);


        Items_Spinner m = Items_Spinners.get(position);

        details.setText(m.getDetails());

        return convertView;
    }

}




/*     public class Spinner_Adapter extends ArrayAdapter<String>
    {

        public Spinner_Adapter(Context context, int textViewResourceId, String[] objects)
        {
            super(context, textViewResourceId, objects);

        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            //return super.getView(position, convertView, parent);

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_item, parent, false);
            TextView label=(TextView)row.findViewById(R.id.txt_spinner);
            // label.setText(DayOfWeek[position]);


            return row;
        }
    }
*/