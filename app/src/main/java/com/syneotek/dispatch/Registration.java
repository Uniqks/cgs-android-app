package com.syneotek.dispatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.adapter.Spinner_Adapter;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.model.Items_Spinner;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registration extends Activity {

    private static final String REGISTER_URL = Const.HOSTNAME + "wsregistration.php?";

    private ArrayList<Items_Spinner> list_itemsSpinners = new ArrayList<Items_Spinner>();
    private Spinner_Adapter adapter;
    public Spinner spinner;

    //    String spinner_value;
    String selected_item;
    String spinner_values;
    int supplier_ID;

    public ArrayList<String> spinner_ID = new ArrayList<>();
    public ArrayList<String> spinner_name = new ArrayList<>();

    Button btn_registration, txt_allreay_acc;

    EditText et_fname, et_lname, et_emailaddress, et_mobile_no, et_password, et_confirmpassword;
    ImageView img_back_arrow;
    TextView txt_title;

    //SPINNER URL AND PARAM
    private static final String SPINNER_URL = Const.HOSTNAME + "wsgetlist.php";
    private static final String TAG = Registration.class.getSimpleName();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Sign Up");

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);

        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        addSpinnerValues();

//        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,);
//        // adapter = new Spinner_Adapter(Registration.this, list_itemsSpinners);
//        spinner.setAdapter(adapter);

//        supplier_ID=spinner.getSelectedItemPosition();
        //       spinner_values=spinner_ID.get(supplier_ID);

        et_fname = (EditText) findViewById(R.id.fname);
        et_lname = (EditText) findViewById(R.id.lname);

        et_emailaddress = (EditText) findViewById(R.id.email);
        et_mobile_no = (EditText) findViewById(R.id.mobileno);

        et_password = (EditText) findViewById(R.id.password);
        et_confirmpassword = (EditText) findViewById(R.id.confirm_password);


        //CLICKED ON ALREADY AN ACCOUNT
        txt_allreay_acc = (Button) findViewById(R.id.btn_allready_acc);
        txt_allreay_acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        //CLICKED EVENT ON REGISTRATION BTN
        btn_registration = (Button) findViewById(R.id.btn_register);
        btn_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    private void registerUser() {
        final String fname = et_fname.getText().toString();
        final String lname = et_lname.getText().toString();
        final String email = et_emailaddress.getText().toString();
        final String phone = et_mobile_no.getText().toString();
        final String password = et_password.getText().toString();
        final String confirm_password = et_confirmpassword.getText().toString();
        //int i=spinner.getSelectedItemPosition();
        //final String str_values = String.valueOf(i);

        // final String spinner_value=spinner.getSelectedItem().toString();

        if (et_fname.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Enter first name", Toast.LENGTH_SHORT).show();
        } else if (et_lname.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Enter last name ", Toast.LENGTH_SHORT).show();
        } else if (et_emailaddress.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Enter email Address", Toast.LENGTH_SHORT).show();
        } else if (et_mobile_no.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (et_password.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Enter password ", Toast.LENGTH_SHORT).show();
        } else if (et_confirmpassword.getText().length() <= 0) {
            Toast.makeText(Registration.this, "Please Confirm the Password ", Toast.LENGTH_SHORT).show();
        } else if (!(password.matches(confirm_password))) {

            Toast.makeText(Registration.this, "Password not matched ", Toast.LENGTH_SHORT).show();
        } else if (!isValidEmail(email)) {

            et_emailaddress.setError("Invalid Email");
        } else if (!isValidMobile(phone)) {

            et_mobile_no.setError("Invalid Phone number");
        } else {
            String admin_id = spinner_ID.get(spinner.getSelectedItemPosition());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, REGISTER_URL + "fname=" + fname + "&lname=" + lname + "&email=" + email + "&mobile=" + phone + "&password=" + password + "&admin=" + admin_id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);

                        String s = object.getString("status");

                        switch (Integer.parseInt(s)) {

                            case 1:
                                Intent intent = new Intent(Registration.this, Login_Activity.class);
                                Toast.makeText(Registration.this, "Registration Is successful ", Toast.LENGTH_LONG).show();
                                startActivity(intent);

                                et_fname.getText().clear();
                                et_lname.getText().clear();
                                et_password.getText().clear();
                                et_mobile_no.getText().clear();
                                et_confirmpassword.getText().clear();
                                et_mobile_no.getText().clear();
                                finish();
                                break;

                            default:
                                Toast.makeText(Registration.this, "Unknown error", Toast.LENGTH_SHORT).show();
                                break;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Registration.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }

    }
//    public void onItemSelected(AdapterView<?> parent, View view, int position,
//                               long id) {
//
//        selected_item = parent.getItemAtPosition(position).toString();
//
//
////        if(selected.equals("SELECT"))
////        {
////
////            Toast.makeText(parent.getContext(),"Enter Your Grade", Toast.LENGTH_LONG).show();
////        }
//    }

    // SPINNER CALL
    private void addSpinnerValues() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final StringRequest jsonObjReq = new StringRequest(Request.Method.GET, SPINNER_URL, new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("info");
                    for (int l = 0; l < jsonArray.length(); l++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(l);
                        // String ID=jsonObject.getString("id");
                        spinner_ID.add(jsonObject.getString("id"));
                        spinner_name.add(jsonObject.getString("name"));
                        //  Items_Spinner data = new Items_Spinner();
                        //  data.setDetails(jsonObject.getString("name"));
                        // list_itemsSpinners.add(data);

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Registration.this, android.R.layout.simple_list_item_1, spinner_name);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Registration.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                // adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Registration.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(jsonObjReq);
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidMobile(String phone_number) {
        String mobile_string = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
        Pattern pattern = Pattern.compile(mobile_string);
        Matcher matcher = pattern.matcher(phone_number);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

}

