package com.syneotek.dispatch.dispatch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.update_profile.Change_Email;
import com.syneotek.dispatch.dispatch.update_profile.Change_MobileNo;
import com.syneotek.dispatch.dispatch.update_profile.Change_Name;

public class Edit_Profile extends Activity {

    ImageView img_back_arrow;
    TextView txt_title, pro_name, pro_email, pro_mob_no;

    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Edit Profile");

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        pro_name = (TextView) findViewById(R.id.edit_profile_name);
        pro_email = (TextView) findViewById(R.id.edit_profile_email);
        pro_mob_no = (TextView) findViewById(R.id.edit_profile_mobileno);

        pro_name.setText(sharedpreferences.getString("name", "No Name"));


        pro_email.setText(sharedpreferences.getString("email", "No Email"));
        pro_mob_no.setText(sharedpreferences.getString("mobile", "No Mobile No"));

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
              //  finish();
               // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);

                //overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });
        RelativeLayout edit_name = (RelativeLayout) findViewById(R.id.user);
        edit_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Edit_Profile.this, Change_Name.class);

                startActivity(intent_smith);
              //  overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);


            }
        });

        RelativeLayout edit_email = (RelativeLayout) findViewById(R.id.email);
        edit_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Edit_Profile.this, Change_Email.class);
                startActivity(intent_smith);
               // overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
            }
        });

        RelativeLayout edit_mobile = (RelativeLayout) findViewById(R.id.mobile);
        edit_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Edit_Profile.this, Change_MobileNo.class);
                startActivity(intent_smith);
              //  overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        pro_name.setText(sharedpreferences.getString("name", "No Name"));
        pro_email.setText(sharedpreferences.getString("email", "No Email"));
        pro_mob_no.setText(sharedpreferences.getString("mobile", "No Mobile No"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
       // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

}



