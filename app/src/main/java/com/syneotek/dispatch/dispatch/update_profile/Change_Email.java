package com.syneotek.dispatch.dispatch.update_profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Syneotek on 3/16/2016.
 */
public class Change_Email extends Activity {

    private static final String TAG = Change_Name.class.getSimpleName();
    private static final String url = Const.HOSTNAME + "wseditProfile.php?";
    public static String uid = "";
    ImageView img_back_arrow;
    TextView txt_title;
    EditText edit_email_hint;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    Button btn_change_email;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_change_email);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Change Email");

        edit_email_hint = (EditText) findViewById(R.id.edit_email_hint);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        edit_email_hint.setText(sharedpreferences.getString("email", "No Email"));


        edit_email_hint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edit_email_hint.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_email_hint.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                edit_email_hint.setError(null);
            }
        });

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);

        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
               // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        btn_change_email = (Button) findViewById(R.id.btn_email_change);
        btn_change_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (edit_email_hint.getText().toString().trim().isEmpty()) {
                    edit_email_hint.setError("Enter Email ID");
                } else {
                    changeEmail();
                }

            }
        });
    }

    void changeEmail() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        uid = sharedpreferences.getString("uid", "0");
        final String str_email_id = edit_email_hint.getText().toString();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final ProgressDialog progressDialog = new ProgressDialog(Change_Email.this);
        if (isValidEmail(str_email_id) && str_email_id.trim().length() > 0) {
            if (isNetworkAvailable()) {
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                final StringRequest jsonObjEmail = new StringRequest(Request.Method.GET, url + "fname=&lname=&email=" + str_email_id + "&mobile=&password=&uid=" + uid, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String jsonstatus = object.getString("status");

                            switch (Integer.parseInt(jsonstatus)) {
                                case 1:
                                    progressDialog.dismiss();
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    editor = sharedpreferences.edit();
                                    editor.putString("email", str_email_id);
                                    editor.commit();
                                    finish();
                                    overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                  //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                    break;
                                default:
                                    progressDialog.dismiss();
                                    Toast.makeText(Change_Email.this, "Error", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(Change_Email.this, "Error while loading data", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(Change_Email.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                    }
                });
                requestQueue.add(jsonObjEmail);
            } else {
                Toast.makeText(Change_Email.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else if (str_email_id.length() == 0) {
            edit_email_hint.setError("Enter Email");
            Toast.makeText(getApplicationContext(), "Please enter your Email ID", Toast.LENGTH_LONG).show();
        } else if (!isValidEmail(str_email_id)) {
            edit_email_hint.setError("Invalid Email");
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager email_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = email_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
       // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

}