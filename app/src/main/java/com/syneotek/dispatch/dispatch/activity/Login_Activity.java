package com.syneotek.dispatch.dispatch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.syneotek.dispatch.Main_Activity_Nevigation;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.Registration;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Syneotek on 3/10/2016.
 */
public class Login_Activity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String SENDER_ID = "376295324751";
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    public static String regid = "";
    static final String TAGgcm = "GCM";

    ImageView img_back_arrow;
    TextView btn_login, btn_sign_up;
    TextView txt_title;

    EditText edt_email, edt_Password;

    public static final String LOGIN_URL = Const.HOSTNAME + "wslogin.php?";

    String TAG_STATUS_CODE = "status";
    String str_email, str_Password;

    String email;

    public static final String MyPREFERENCES = "MyPrefs";

    public static SharedPreferences sharedpreferences, sh;
    public static SharedPreferences.Editor editor, edtr;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Login");

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);

        edt_email = (EditText) findViewById(R.id.email);
        edt_Password = (EditText) findViewById(R.id.et_password);

        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        btn_login = (TextView) findViewById(R.id.btn_login);
        btn_sign_up = (TextView) findViewById(R.id.btn_sign_up);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_email = edt_email.getText().toString().trim();
                str_Password = edt_Password.getText().toString().trim();

                if (isValidEmail(str_email) && str_email.trim().length() > 0 && str_Password.trim().length() > 0) {
                    userLogin(str_email, str_Password);
                } else if (str_email.length() == 0 && str_Password.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please enter your login User Name and Password", Toast.LENGTH_LONG).show();
                } else if (str_email.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please enter your User Name", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(str_email)) {
                    edt_email.setError("Invalid Email");
                } else if (str_Password.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please enter your Password", Toast.LENGTH_LONG).show();
                }

               /* Intent intent = new Intent(Login_Activity.this, Main_Activity_Nevigation.class);
                startActivity(intent);*/
                //overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                //overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                //finish();
            }
        });
        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login_Activity.this, Registration.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
            }
        });

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(Login_Activity.this);
            regid = getRegistrationId(Login_Activity.this);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        }
    }

    public void userLogin(String emailID, final String password) {

        if (isNetworkAvailable()) {
            final ProgressDialog progressDialog = new ProgressDialog(Login_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            StringRequest stringappLoginRequest = new StringRequest(Request.Method.GET,
                    LOGIN_URL + "email=" + emailID + "&password=" + password + "&deviceid=" + regid +
                            "&type=A", new Response.Listener<String>() {

                public void onResponse(String response) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);
                        String data = jsonObj.getString(TAG_STATUS_CODE);
                        switch (Integer.parseInt(data)) {
                            case 1:
                                JSONObject objUser = jsonObj.getJSONObject("info");
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                editor = sharedpreferences.edit();
                                editor.putString("uid", objUser.getString("id"));
                                editor.putString("name", objUser.getString("name"));

                                editor.putString("fname", objUser.getString("fname"));
                                editor.putString("lname", objUser.getString("lname"));

                                editor.putString("email", objUser.getString("email"));
                                editor.putString("mobile", objUser.getString("mobile"));
                                editor.putString("pass", password);
                                editor.commit();
                                Intent intent = new Intent(Login_Activity.this, Main_Activity_Nevigation.class);
                                startActivity(intent);
                                Toast.makeText(Login_Activity.this, "Successfully Logined!", Toast.LENGTH_LONG).show();
                                finish();
                                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                                break;
                            default:
                                Toast.makeText(Login_Activity.this, "please enter valid username/password!", Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(Login_Activity.this, error.toString(), Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringappLoginRequest);
        } else {
            Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager app_login_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = app_login_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

    protected void onResume() {
        super.onResume();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(Login_Activity.this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, Login_Activity.this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAGgcm, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAGgcm, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAGgcm, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(Login_Activity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @SuppressWarnings("unchecked")
    private void registerInBackground() {
        new AsyncTask() {
            protected void onPostExecute(Object msg) {
                // mDisplay.append(msg + "\n");
                if (regid.isEmpty()) {

//                        registerInBackground();

                }
            }

            ;

            @Override
            protected Object doInBackground(Object... arg0) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(Login_Activity.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over
                    // HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your
                    // app.
                    // The request to your server should be authenticated if
                    // your app
                    // is using accounts.
                    // sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the
                    // device
                    // will send upstream messages to a server that echo back
                    // the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    if (!regid.isEmpty()) {
                        storeRegistrationId(Login_Activity.this, regid);
                    }
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }
        }.execute(null, null, null);

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAGgcm, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
