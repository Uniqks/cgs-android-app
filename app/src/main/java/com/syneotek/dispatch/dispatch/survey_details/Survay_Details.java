package com.syneotek.dispatch.dispatch.survey_details;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.syneotek.dispatch.BuildConfig;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.activity.Help;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.dispatch.activity.Take_Picture;
import com.syneotek.dispatch.dispatch.activity.ViewUploadedImagesActivity;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.syneotek.dispatch.dispatch.activity.Take_Picture.IMAGEFOLDER;
import static com.syneotek.dispatch.dispatch.activity.Take_Picture.createScaledBitmap;
import static com.syneotek.dispatch.dispatch.activity.Take_Picture.getImagePath;

/**
 * Created by Syneotek on 3/16/2016.
 */
public class Survay_Details extends AppCompatActivity {

    private String KEY_IMAGE = "image";
    String info;
    ProgressDialog loading;

    private static final String TAG = Survay_Details.class.getSimpleName();
    public static LatLng my_loc, job_loc, other_loc, home_loc;
    private GoogleMap googleMap;
    FragmentTransaction fragmentTransaction_map;
    GPSTracker gps;
    RelativeLayout relative_searching, relative_map, rel_accept, rel_complete;
    FrameLayout frame_bottom;
    TextView txt_accept_yes, txt_accept_no;
    private static final String url = Const.HOSTNAME + "wsupstatus.php?";
    private static final String subInstallerUrl = Const.HOSTNAME + "wssubinstallerupstatus.php?";
    private static final String download_url = Const.HOSTNAME + "wsdownloadurl_one.php?";
    private String UPLOAD_URL = Const.HOSTNAME + "uploadprofileimage.php";
    private String KEY_SIGNATURE = "image";
    String extraURL = Const.HOSTNAME + "addextra.php?";
    boolean resGet = true;
    public static String uid = "";
    String TAG_STATUS_CODE = "status";
    final int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_FILE2 = 2;
    Bitmap bitmap;
    ImageView extraImage;
    ImageView img_back_arrow;
    TextView txt_title, txt_mark_as_complaint, txt_help;
    public static String job_id = "", details = "", address = "", lat = "", lango = "",
            com_status = "", ass_date = "", workingDate = "", certificate = "",
            description = "", contact = "", from_time = "", to_time = "", po = "",
            store = "", phone = "", attachment = "", price = "";
    public static String olat = "", olng = "", hlat = "", hlng = "";
    public static int stype;

    TextView txt_name, txt_address, txt_workingdate, txt_status, btn_download_com, btn_details, priceText;
    Integer[] array_btn_id = {R.id.btn_download_one, R.id.btn_download_two, R.id.btn_download_three,
            R.id.btn_download_four, R.id.btn_download_five,
            R.id.btn_download_six, R.id.btn_download_seven, R.id.btn_download_eight,
            R.id.btn_download_nine, R.id.btn_download_ten, R.id.btn_download_eleven, R.id.btn_download_twelve
    };
    LinearLayout lin_download_btn, attachmentLayout, addExtra;
    Button btnViewImages;
    String images;
    Uri imageURI;

    String mCurrentPhotoPath;


    //Button btn_mark_as_complaint;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        setContentView(R.layout.survey_details);
        Const.verifyCallPermissions(Survay_Details.this);
        resGet = true;

        btnViewImages = (Button) findViewById(R.id.btnViewImages);
        rel_complete = (RelativeLayout) findViewById(R.id.rel_complete);
        rel_complete.setVisibility(RelativeLayout.VISIBLE);

        relative_searching = (RelativeLayout) findViewById(R.id.loc_relative_searching);
        relative_searching.setVisibility(RelativeLayout.GONE);

        relative_map = (RelativeLayout) findViewById(R.id.relative_map);
        relative_map.setVisibility(RelativeLayout.VISIBLE);

        rel_accept = (RelativeLayout) findViewById(R.id.rel_accept);
        rel_accept.setVisibility(RelativeLayout.GONE);

        frame_bottom = (FrameLayout) findViewById(R.id.frame_bottom);
        frame_bottom.setVisibility(FrameLayout.VISIBLE);

        btn_download_com = (TextView) findViewById(R.id.btn_download_com);
        //  btn_download_com.setVisibility(TextView.INVISIBLE);

        lin_download_btn = (LinearLayout) findViewById(R.id.lin_download_btn);
        lin_download_btn.setVisibility(LinearLayout.GONE);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Job Details");

        addExtra = (LinearLayout) findViewById(R.id.addExtra);
        attachmentLayout = (LinearLayout) findViewById(R.id.attachment);
        priceText = (TextView) findViewById(R.id.price);
//        attachmentLayout.setVisibility(View.GONE);

        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_address = (TextView) findViewById(R.id.txt_address);
        txt_workingdate = (TextView) findViewById(R.id.txt_time);
        txt_status = (TextView) findViewById(R.id.txt_status);
        btn_details = (TextView) findViewById(R.id.btn_details);

        txt_accept_yes = (TextView) findViewById(R.id.txt_yes);
        txt_accept_no = (TextView) findViewById(R.id.txt_no);

        Intent intent = getIntent();
        job_id = intent.getStringExtra("Job_ID");
        details = intent.getStringExtra("Details");
        txt_name.setText(details);
        address = intent.getStringExtra("Address");
        txt_address.setText(address);

        lat = intent.getStringExtra("Lat");
        lango = intent.getStringExtra("Lang");

        if (!lat.equals("") && !lango.equals(""))
            job_loc = new LatLng(Double.valueOf(lat), Double.valueOf(lango));


        olat = intent.getStringExtra("olat");
        olng = intent.getStringExtra("olng");


        if (!olat.equals("") && !olng.equals("") && !olat.equals("0") && !olng.equals("0")) {
            other_loc = new LatLng(Double.valueOf(olat), Double.valueOf(olng));
        }

        /*if (!olat.equals("") && !olng.equals("")) {
            other_loc = new LatLng(Double.valueOf(olat), Double.valueOf(olng));
        }
*/
        hlat = intent.getStringExtra("hlat");
        hlng = intent.getStringExtra("hlng");

        if (!hlat.equals("") && !hlng.equals(""))
            home_loc = new LatLng(Double.valueOf(hlat), Double.valueOf(hlng));


        ass_date = intent.getStringExtra("Assigneddate");
        workingDate = intent.getStringExtra("Workinddate");


        //  txt_workingdate.setText(workingDate);
        // txt_workingdate.setText(workingDate);
        certificate = intent.getStringExtra("Certificate");
        description = intent.getStringExtra("Description");
        contact = intent.getStringExtra("contact");
        from_time = intent.getStringExtra("from_time");
        to_time = intent.getStringExtra("to_time");
        po = intent.getStringExtra("po");
        store = intent.getStringExtra("store");
        phone = intent.getStringExtra("contact");
        attachment = intent.getStringExtra("attachment");
        price = intent.getStringExtra("price");
        stype = intent.getIntExtra("stype", 0);

        txt_workingdate.setText(from_time + "-" + to_time);
//        stype= Integer.parseInt(sType);
     /*   if (!certificate.equalsIgnoreCase("no")) {
            btn_download_com.setVisibility(TextView.VISIBLE);
        } else {
            btn_download_com.setVisibility(TextView.INVISIBLE);
        }*/

        com_status = intent.getStringExtra("Status");
        if (com_status.equalsIgnoreCase("c")) {
            txt_status.setText("Completed");
            btn_download_com.setVisibility(TextView.VISIBLE);
//            txt_status.setTextColor(Color.parseColor("#138808"));
            relative_map.setVisibility(RelativeLayout.GONE);
            frame_bottom.setVisibility(FrameLayout.GONE);
            rel_accept.setVisibility(RelativeLayout.GONE);
            rel_complete.setVisibility(RelativeLayout.GONE);
            lin_download_btn.setVisibility(LinearLayout.VISIBLE);
//            btnViewImages.setVisibility(View.VISIBLE);
            addExtra.setVisibility(View.GONE);
            //   getDownloadLinks();
        } else if (com_status.equalsIgnoreCase("p")) {
            txt_status.setText("Pending");
            relative_map.setVisibility(RelativeLayout.VISIBLE);
            frame_bottom.setVisibility(FrameLayout.VISIBLE);
            rel_accept.setVisibility(RelativeLayout.GONE);
            rel_complete.setVisibility(RelativeLayout.VISIBLE);
            lin_download_btn.setVisibility(LinearLayout.GONE);
//            txt_status.setTextColor(Color.parseColor("#FF0"));
        } else if (com_status.equalsIgnoreCase("a")) {
            txt_status.setText("Created");
            //  btn_download_com.setVisibility(TextView.INVISIBLE);
            relative_map.setVisibility(RelativeLayout.VISIBLE);
            frame_bottom.setVisibility(FrameLayout.VISIBLE);
            rel_accept.setVisibility(RelativeLayout.VISIBLE);
            rel_complete.setVisibility(RelativeLayout.GONE);
            lin_download_btn.setVisibility(LinearLayout.GONE);
//            txt_status.setTextColor(Color.parseColor("#FF0"));
        } else if (com_status.equalsIgnoreCase("ne")) {
            txt_status.setText("Not Excepted");
            relative_map.setVisibility(RelativeLayout.VISIBLE);
            frame_bottom.setVisibility(FrameLayout.VISIBLE);
            rel_accept.setVisibility(RelativeLayout.GONE);
            rel_complete.setVisibility(RelativeLayout.GONE);
            lin_download_btn.setVisibility(LinearLayout.GONE);
//            txt_status.setTextColor(Color.parseColor("#ff0000"));
        } else if (com_status.equalsIgnoreCase("e")) {
            txt_status.setText("Accepted");
            btn_download_com.setVisibility(TextView.VISIBLE);
            relative_map.setVisibility(RelativeLayout.VISIBLE);
            frame_bottom.setVisibility(FrameLayout.VISIBLE);
            rel_accept.setVisibility(RelativeLayout.GONE);
            rel_complete.setVisibility(RelativeLayout.VISIBLE);
            lin_download_btn.setVisibility(LinearLayout.GONE);
//            txt_status.setTextColor(Color.parseColor("#ffa500"));
        }

        if (stype == 1) {
            addExtra.setVisibility(View.GONE);
            rel_complete.setVisibility(RelativeLayout.GONE);
        }


        if (isNetworkAvailable())
            setUpMap();
        else
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection !", Toast.LENGTH_SHORT).show();

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
                // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        txt_mark_as_complaint = (TextView) findViewById(R.id.txt_mark);
        txt_mark_as_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Survay_Details.this, Take_Picture.class);
                startActivity(intent_smith);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                // overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                finish();
            }
        });

        txt_help = (TextView) findViewById(R.id.txt_help_com);
        txt_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Survay_Details.this, Help.class);
                intent_smith.putExtra("Job_ID", job_id);
                startActivity(intent_smith);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                // overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                finish();
            }
        });

        txt_accept_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stype == 0)
                    startDialogSign();
                else
                    subInstallerCall("E", "");
            }
        });

        txt_accept_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stype == 0)
                    startDialog(0);
                else
                    startDialog(1);
            }
        });

        btn_download_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDownloadLinks();
              /*  Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(certificate));
                startActivity(i);*/
            }
        });


        btn_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDialogDeatils();

//                if (description.equalsIgnoreCase("")) {
//                    showDialogMsg("Details", "No Details Available !", "Close");
//                } else {
//                    showDialogMsg("Details", description, "Close");
//                }
            }
        });

        price = "$" + price;
        priceText.setText(price);
//        priceText.setVisibility(View.GONE);
        if (attachment.equalsIgnoreCase("no") || attachment.equals("")) {
            attachmentLayout.setVisibility(View.GONE);
        }
        attachmentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (attachment.equals("")) {
//                    Toast.makeText(Survay_Details.this, "No attachment", Toast.LENGTH_SHORT).show();
//                } else {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(attachment));
                startActivity(i);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                }
            }
        });

        addExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddExtraDialog();
            }
        });

        btnViewImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Survay_Details.this, ViewUploadedImagesActivity.class);
                intent.putExtra("Images", images);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
            }
        });
    }

    void accept_job(final String status, String reason, String sign) {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Survay_Details.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_no = new StringRequest(Request.Method.GET, url + "driver_id=" + uid + "&job_id=" + job_id + "&status=" + status + "&reason=" + reason + "&sign=" + sign, new Response.Listener<String>() {
                public void onResponse(String response) {

                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                rel_accept.setVisibility(RelativeLayout.GONE);
                                rel_complete.setVisibility(RelativeLayout.VISIBLE);
                                if (status.equalsIgnoreCase("ne")) {
                                    finish();
                                    overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                    //overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                }
                                break;
                            default:
                                Toast.makeText(Survay_Details.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonReq_no);

        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    void subInstallerCall(final String status, String reason) {
        if (isNetworkAvailable()) {

            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Survay_Details.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            final StringRequest jsonReq_no = new StringRequest(Request.Method.GET, subInstallerUrl + "driver_id=" + uid + "&job_id=" + job_id + "&status=" + status + "&reason=" + reason, new Response.Listener<String>() {
                public void onResponse(String response) {

                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                rel_accept.setVisibility(RelativeLayout.GONE);
//                                rel_complete.setVisibility(RelativeLayout.VISIBLE);
                                if (status.equalsIgnoreCase("ne")) {
                                    finish();
                                    overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                    //overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                }
                                break;
                            default:
                                Toast.makeText(Survay_Details.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonReq_no);

        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void showDialogMsg(String title, String msg, String action) {
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(Survay_Details.this);
        dialog.setTitle(title);
        dialog.setMessage(msg);
        dialog.setPositiveButton(action,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.setCancelable(false);
        dialog.show();
    }

    void startDialog(final int value) {
        final Dialog dialog = new Dialog(Survay_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.reson_dialog);
        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final EditText ed_reason = (EditText) dialog.findViewById(R.id.ed_reason);
        ed_reason.setLines(5);
        ed_reason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_reason.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!relativeLayout_footer.isShown()) {
//                    relativeLayout_footer.setVisibility(RelativeLayout.VISIBLE);
//                    ed_odo_read.getText().clear();
//                    ed_odo_read.setHint("Starting Odometer Reading");
//
//                }
                if (!ed_reason.getText().toString().trim().isEmpty()) {
                    dialog.dismiss();
                    if (value == 0)
                        accept_job("NE", ed_reason.getText().toString().replace(" ", "%20"), "");
                    else
                        subInstallerCall("NE", ed_reason.getText().toString().replace(" ", "%20"));
                } else {
                    ed_reason.setError("Enter reason !");
                }
            }
        });

        TextView txt_cancle = (TextView) dialog.findViewById(R.id.txt_cancle);
        txt_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    void getDownloadLinks() {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            String uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Survay_Details.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_link = new StringRequest(Request.Method.GET, download_url + "driver_id=" + uid + "&job_id=" + Survay_Details.job_id, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    Log.e("Links", response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");
                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:

                                JSONArray array = object.getJSONArray("info");
                                if (array.length() > 0)
                                    showAddExtraDialog1(object);
                                else
                                    Toast.makeText(Survay_Details.this, "No data found please try again!", Toast.LENGTH_LONG).show();

                                break;
                            default:
                                Toast.makeText(Survay_Details.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonReq_link);

        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void showAddExtraDialog1(JSONObject object) {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(Survay_Details.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_show_certificate, null);
        dialogBuilder.setView(dialog);
        dialogBuilder.setCancelable(true);

//        final Dialog dialog = new Dialog(Signature_Activity.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_show_certificate);
//        dialog.setCancelable(true);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final android.support.v7.app.AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);

        JSONArray array = null;
        try {
            array = object.getJSONArray("info");
//            images = object.getString("image");
            for (int i = 0; i < array.length(); i++) {
                final JSONObject object1 = array.getJSONObject(i);
                if (i < array_btn_id.length) {
                    Button button = (Button) dialog.findViewById(array_btn_id[i]);
                    if (button != null) {
                        if (!object1.getString("invoice_type").equalsIgnoreCase("")) {
                            button.setVisibility(Button.VISIBLE);
                            button.setText(object1.getString("invoice_type"));
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String url = null;
                                    try {
                                        url = object1.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);
                                    b.dismiss();
                                }
                            });
                        } else {
                            button.setVisibility(Button.GONE);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

    }


/*    void getDownloadLinks() {
        if (isNetworkAvailable()) {
            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Survay_Details.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_link = new StringRequest(Request.Method.GET, download_url + "driver_id=" + uid + "&job_id=" + job_id, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    Log.e("Links", response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                JSONArray array = object.getJSONArray("info");
                                images = object.getString("image");
                                for (int i = 0; i < array.length(); i++) {
                                    final JSONObject object1 = array.getJSONObject(i);

                                    if (i < array_btn_id.length) {
                                        Button button = (Button) findViewById(array_btn_id[i]);
                                        if (button != null) {
                                            if (!object1.getString("invoice_type").equalsIgnoreCase("")) {
                                                button.setVisibility(Button.VISIBLE);
                                                button.setText(object1.getString("invoice_type"));
                                                button.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        String url = null;
                                                        try {
                                                            url = object1.getString("url");
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                                        i.setData(Uri.parse(url));
                                                        startActivity(i);
                                                    }
                                                });
                                            } else {
                                                button.setVisibility(Button.GONE);
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                Toast.makeText(Survay_Details.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Survay_Details.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            App.getInstance().addToRequestQueue(jsonReq_link, "jsonReg_link");

        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }*/

    void setUpMap() {
        try {
            if (googleMap == null) {
//                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.google_map_survay)).getMap();
                googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map_survay)).getMap();
            }
            // googleMap.setMyLocationEnabled(true);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            gps = new GPSTracker(Survay_Details.this);
            if (gps.canGetLocation()) {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                my_loc = new LatLng(latitude, longitude);


                Marker TP = googleMap.addMarker(new MarkerOptions()
                        .position(my_loc).title("My Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                        .snippet(getCompleteAddressString(latitude, longitude)).visible(true)
                );
                if (!lat.equalsIgnoreCase("") && !lat.equalsIgnoreCase("0") && !lango.equalsIgnoreCase("") && !lango.equalsIgnoreCase("0")
                        && !address.equalsIgnoreCase("")) {
                    Marker TP_Job = googleMap.addMarker(new MarkerOptions()
                            .position(job_loc).title("Job Location")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                            .snippet(address)
                    );
                }


                if (!olat.equalsIgnoreCase("") && !olat.equalsIgnoreCase("0") && !olng.equalsIgnoreCase("") && !olng.equalsIgnoreCase("0")
                        && !address.equalsIgnoreCase("")) {
                    Marker TP_Other = googleMap.addMarker(new MarkerOptions()
                            .position(other_loc).title("Other Location")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                            .snippet(address)
                    );
                }


                if (!hlat.equalsIgnoreCase("") && !hlat.equalsIgnoreCase("0") && !hlng.equalsIgnoreCase("") && !hlng.equalsIgnoreCase("0")
                        && !address.equalsIgnoreCase("")) {
                    Marker TP_Other = googleMap.addMarker(new MarkerOptions()
                            .position(home_loc).title("Home Location")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .snippet(address)
                    );
                }


                if (googleMap != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(my_loc, 15.0f));
                    String urll = makeURL(latitude, longitude, Double.valueOf(lat), Double.valueOf(lango));
                    //  getDrawPath(urll);
                }
            } else {
                gps.showSettingsAlert();
            }
            googleMap.getUiSettings().setZoomControlsEnabled(true);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void startDialogDeatils() {
        final Dialog dialog = new Dialog(Survay_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_details);
        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView txt_view_msg = (TextView) dialog.findViewById(R.id.txt_view_msg);
        TextView txt_name = (TextView) dialog.findViewById(R.id.txt_name);
        TextView txt_address = (TextView) dialog.findViewById(R.id.txt_address);
        TextView txt_date = (TextView) dialog.findViewById(R.id.txt_date);
        TextView txt_time = (TextView) dialog.findViewById(R.id.txt_time);
        TextView txt_status = (TextView) dialog.findViewById(R.id.txt_status);
        TextView txt_phone = (TextView) dialog.findViewById(R.id.txt_phone);
        TextView txt_po = (TextView) dialog.findViewById(R.id.txt_po);
        TextView txt_store = (TextView) dialog.findViewById(R.id.txt_store);
        TextView txt_details = (TextView) dialog.findViewById(R.id.txt_details);

        txt_name.setText(details);
        txt_address.setText(address);
        txt_date.setText("From " + ass_date + " To " + workingDate);
        txt_time.setText("From " + from_time + " To " + to_time);

        if (com_status.equalsIgnoreCase("c")) {
            txt_status.setText("Completed");
        } else if (com_status.equalsIgnoreCase("p")) {
            txt_status.setText("Pending");
        } else if (com_status.equalsIgnoreCase("a")) {
            txt_status.setText("Created");
        } else if (com_status.equalsIgnoreCase("ne")) {
            txt_status.setText("Not Excepted");
        } else if (com_status.equalsIgnoreCase("e")) {
            txt_status.setText("Accepted");
        }

        if (phone.trim().isEmpty()) {
            txt_phone.setText("123456789");
        } else {
            txt_phone.setText(phone);
        }
        txt_po.setText(po);
        txt_store.setText(store);
        txt_details.setText(description);

        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txt_view_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Survay_Details.this, Help.class);
                intent_smith.putExtra("Job_ID", job_id);
                startActivity(intent_smith);

                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                //   overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                dialog.dismiss();
            }
        });

        txt_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                if (ActivityCompat.checkSelfPermission(Survay_Details.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

            }
        });
    }

    void startDialogSign() {
        final Dialog dialog = new Dialog(Survay_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_signature);
        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView txt_done = (TextView) dialog.findViewById(R.id.txt_done);
        final GestureOverlayView gesture = (GestureOverlayView) dialog.findViewById(R.id.signature_box);

        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    gesture.setDrawingCacheEnabled(true);
                    Bitmap gestureImg = Bitmap.createBitmap(gesture.getDrawingCache());
//                    iv.setImageBitmap(b);
                    gesture.setDrawingCacheEnabled(false);

//                    Bitmap gestureImg = gesture.getGesture().toBitmap(100, 100, 8,Color.WHITE);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    gestureImg.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    byte[] bArray = bos.toByteArray();
                    if (gestureImg != null) {
                        uploadSignature(gestureImg);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(Survay_Details.this, "Sign not added ! Please try again ...", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Survay_Details.this, "Sign not added ! Please try again ...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void uploadSignature(final Bitmap bitmap) {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringDiaSignRequest = new StringRequest(Request.Method.POST, UPLOAD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("AddImageSignature", response);
                try {
                    JSONObject object = new JSONObject(response);
                    String data = object.getString(TAG_STATUS_CODE);
                    switch (Integer.parseInt(data)) {
                        case 1:
                            String img_name = object.getString("info");
                            loading.dismiss();
                            Toast.makeText(Survay_Details.this, "Signature Uploaded !", Toast.LENGTH_LONG).show();
                            accept_job("E", "", img_name);
                            break;
                        default:
                            loading.dismiss();
                            Toast.makeText(Survay_Details.this, "Error:", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    loading.dismiss();
                    e.printStackTrace();
                    Toast.makeText(Survay_Details.this, "Error: ", Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(Survay_Details.this, "Please Do Signature", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String image = getStringImage(bitmap);
                Map<String, String> params = new Hashtable<String, String>();
                params.put(KEY_SIGNATURE, image);
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringDiaSignRequest);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                StringBuilder strReturnedAddress = new StringBuilder("");
                strReturnedAddress.append(addresses.get(0).getAddressLine(0)).append(",");
                strReturnedAddress.append(addresses.get(0).getLocality()).append(",");
                strReturnedAddress.append(addresses.get(0).getAdminArea()).append(",");
                strReturnedAddress.append(addresses.get(0).getCountryName());
//                strReturnedAddress.append(addresses.get(0).getPostalCode()).append(",");
//                strReturnedAddress.append(addresses.get(0).getFeatureName());
                strAdd = strReturnedAddress.toString();
                Log.w("MyCurrentloctionaddress", "" + strReturnedAddress.toString());
            } else {
                Log.w("MyCurrentloctionaddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("MyCurrentloctionaddress", "Canont get Address!");
        }
        return strAdd;
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    public String makeURL(double sourcelat, double sourcelog, double destlat,
                          double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }

    void getDrawPath(String url) {
        showProgressDialog();
        if (resGet) {
            resGet = false;
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            String tag_json_obj = "map_req";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        Log.d(TAG, response);
//                        toast(response);

                        hideProgressDialog();
                        drawPath(response);
                        JSONObject jsonObj = new JSONObject(response);
//                    String login_status = jsonObj.getString(TAG_STATUS_CODE);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    hideProgressDialog();
                }
            });
            requestQueue.add(strReq);
        } else {
            hideProgressDialog();
        }
    }

    public void drawPath(String result) {


        Polyline line = null;
        LatLng src = null, dest = null;
        try {
            // Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes
                    .getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);

            for (int z = 0; z < list.size() - 1; z++) {
                src = list.get(z);
                dest = list.get(z + 1);

                line = googleMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(src.latitude, src.longitude),
                                new LatLng(dest.latitude, dest.longitude))
                        .width(5).color(Color.BLUE).geodesic(true));

            }
            hideProgressDialog();
        } catch (JSONException e) {

            hideProgressDialog();
        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public void showProgressDialog() {
        relative_searching.setVisibility(RelativeLayout.GONE);
    }

    public void hideProgressDialog() {
        try {
            if (relative_searching.isShown())
                relative_searching.setVisibility(RelativeLayout.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void showAddExtraDialog() {
        final Dialog dialog = new Dialog(Survay_Details.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_extra_add);
        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        final EditText price = (EditText) dialog.findViewById(R.id.price);
        final EditText comment = (EditText) dialog.findViewById(R.id.comment);
        TextView submit = (TextView) dialog.findViewById(R.id.submit);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        extraImage = (ImageView) dialog.findViewById(R.id.image);

        extraImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                uploadExtra(price.getText().toString().trim(), comment.getText().toString().trim());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = null;
                dialog.dismiss();
            }
        });

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(Survay_Details.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);

                    Intent getCameraImage = new Intent("android.media.action.IMAGE_CAPTURE");

                    File cameraFolder;
                    //Check to see if there is an SD card mounted
                    if (Environment.getExternalStorageState().equals
                            (Environment.MEDIA_MOUNTED))
                        cameraFolder = new File(Environment.getExternalStorageDirectory(),
                                IMAGEFOLDER);
                    else
                        cameraFolder = Survay_Details.this.getCacheDir();
                    if (!cameraFolder.exists())
                        cameraFolder.mkdirs();

                    //Appending timestamp to "picture_"
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
                    String timeStamp = dateFormat.format(new Date());
                    String imageFileName = "picture_" + timeStamp + ".jpg";


                    //-----------------22-1-2018---------------

                    /*  File photo = new File(Environment.getExternalStorageDirectory(),
                            IMAGEFOLDER + imageFileName);*/
                    File photo = new File(Environment.getExternalStorageDirectory(),
                            IMAGEFOLDER + "/" + imageFileName);
                    imageURI = FileProvider.getUriForFile(Survay_Details.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            photo);

                    mCurrentPhotoPath = "file:" + photo.getAbsolutePath();

                    getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                    getCameraImage.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    //  getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                    //Setting a global variable to be used in the OnActivityResult
                    //imageURI = Uri.fromFile(photo);

                    //-------------

                    //getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                    //Setting a global variable to be used in the OnActivityResult
                    // imageURI = Uri.fromFile(photo);
                    startActivityForResult(getCameraImage, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Gallery")) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                    startActivityForResult(galleryIntent, SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void uploadImage(final Bitmap bitmap) {
        //Showing the progress dialog
        if (isNetworkAvailable()) {

            loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringPicRequest = new StringRequest(Request.Method.POST, UPLOAD_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString(TAG_STATUS_CODE);
                        switch (Integer.parseInt(data)) {
                            case 1:
                                String info = object.getString("info");
                                loading.dismiss();
                                Toast.makeText(Survay_Details.this, "Image Uploaded !", Toast.LENGTH_LONG).show();

//                                if (bitmap != null) {
//                                    bitmap.recycle();
//                                    bitmap = null;
//                                }

                                break;
                            default:
                                loading.dismiss();
                                Toast.makeText(Survay_Details.this, "Error:", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error: ", Toast.LENGTH_LONG).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            loading.dismiss();
                            Toast.makeText(Survay_Details.this, "Please Select Image ", Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    String image = getStringImage(bitmap);
                    Map<String, String> params = new Hashtable<String, String>();
                    params.put(KEY_IMAGE, image);

                    //returning parameters
                    return params;
                }
            };

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringPicRequest);
        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
/*

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CAMERA)rgetName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmapOptions.inSampleSize = 8;
                    */
/*bitmapCamera = BitmapFactory.decodeFile(
                            f.getAbsolutePath(), bitmapOptions);*//*

                    bitmap = decodeFile(f, 200);
                    fixOrientation();
//                    saveimage = bitmapCamera;
                    uploadImage(bitmap);
                    extraImage.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                new ImageUploadTask(getActivity()).execute();
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.MediaColumns.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
//                saveimage = bitmapgallery;
//                new ImageUploadTask(getActivity()).execute();
                uploadImage(bitmap);
                extraImage.setImageBitmap(bitmap);
            }
        }
*/

        if (resultCode == RESULT_OK) {


            switch (requestCode) {
                case SELECT_FILE:
                    try {
                        Bitmap bitmap = createScaledBitmap(getImagePath(data, getApplicationContext()), 200, 200);
//                    imageView.setImageBitmap(bitmap);
                        ExifInterface ei = new ExifInterface(getImagePath(data, getApplicationContext()));
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                bitmap = Const.rotateImage(bitmap, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                bitmap = Const.rotateImage(bitmap, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                bitmap = Const.rotateImage(bitmap, 270);
                                break;
                            case ExifInterface.ORIENTATION_NORMAL:
                                break;
                            default:
                                break;
                        }

                        extraImage.setImageBitmap(bitmap);
                        uploadImage(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case REQUEST_CAMERA:
                    try {

                        imageURI = Uri.parse(mCurrentPhotoPath);
                        String path = imageURI.getPath();

                       // String path = imageURI.getPath();
                        Bitmap bitmapCamera = createScaledBitmap(path, 200, 200);

                        ExifInterface ei = new ExifInterface(path);
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 270);
                                break;
                            case ExifInterface.ORIENTATION_NORMAL:
                                break;
                            default:
                                break;
                        }

                        extraImage.setImageBitmap(bitmapCamera);
                        uploadImage(bitmapCamera);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }

    private void uploadExtra(String price, String comment) {
        if (isNetworkAvailable()) {
            String base64String = "";
            if (bitmap != null) {
                base64String = getBase64String(bitmap);
            }
//        else {
//            Toast.makeText(this, "Some Error Occured", Toast.LENGTH_SHORT)
//                    .show();
//            return;
//        }
            //Showing the progress dialog
            SharedPreferences sharedpreferences = getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            String uid = sharedpreferences.getString("uid", "0");
            RequestQueue requestQueue = Volley.newRequestQueue(this);

//        jid=1&cost=23&comment=gfgkh&uid=1
            final ProgressDialog loading = ProgressDialog.show(this, "Submitting...", "Please wait...", false, false);
            StringRequest stringFinishRequest = new StringRequest(Request.Method.POST, extraURL
                    + "jid=" + job_id
                    + "&cost=" + price
                    + "&comment=" + comment
                    + "&uid=" + uid
                    + "&image=" + info
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString(TAG_STATUS_CODE);
                        switch (Integer.parseInt(data)) {
                            case 1:
                                Toast.makeText(Survay_Details.this, "Submitted!", Toast.LENGTH_LONG).show();
                                bitmap = null;
                                loading.dismiss();
                                break;
                            default:
                                loading.dismiss();
                                Toast.makeText(Survay_Details.this, "Error:", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Survay_Details.this, "Error: ", Toast.LENGTH_LONG).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            loading.dismiss();
                            try {
                                Log.e("Error", volleyError.getMessage());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(Survay_Details.this, "Error: ", Toast.LENGTH_LONG).show();
                        }
                    });

            stringFinishRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //Creating a Request Queue
            requestQueue.add(stringFinishRequest);
        } else {
            Toast.makeText(Survay_Details.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public static final String getBase64String(Bitmap photoBitmap) {
        String photo;
        if (photoBitmap != null) {

            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, bao);
            // photoBitmap.recycle();
            photo = Base64.encodeToString(bao.toByteArray(),
                    Base64.DEFAULT);
            try {
                bao.close();
                bao = null;
                photoBitmap = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            photo = "";
        }
        return photo;
    }

    public void fixOrientation() {
        if (bitmap.getWidth() > bitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), matrix,
                    true);
        }
    }

    public static Bitmap decodeFile(File f, int IMAGE_MAX_SIZE) {
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize

        try {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return b;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
        //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager sur_no = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = sur_no
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
