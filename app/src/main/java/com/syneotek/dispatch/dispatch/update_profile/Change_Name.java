package com.syneotek.dispatch.dispatch.update_profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Syneotek on 3/16/2016.
 */
public class Change_Name extends Activity {

    private static final String TAG = Change_Name.class.getSimpleName();
    private static final String url = Const.HOSTNAME + "wseditProfile.php?";
    public static String uid = "";
    ImageView img_back_arrow;
    TextView txt_title;
    Button btn_change_name;
    EditText edit_fname, edit_lname;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_chnage_name);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Change Name");

        edit_fname = (EditText) findViewById(R.id.edit_fname);
        edit_lname = (EditText) findViewById(R.id.edit_lname);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        String firstName=sharedpreferences.getString("fname", "No Name");
        String lastName=sharedpreferences.getString("lname", "No Name");

        edit_fname.setText(firstName);
        edit_lname.setText(lastName);

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);

        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
               // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });


        edit_fname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edit_fname.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_fname.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                edit_fname.setError(null);
            }
        });

        edit_lname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edit_lname.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_lname.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                edit_lname.setError(null);
            }
        });

        btn_change_name = (Button) findViewById(R.id.btn_name_change);
        btn_change_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edit_fname.getText().toString().trim().isEmpty()) {
                    edit_fname.setError("Enter First Name");
                } else if (edit_lname.getText().toString().trim().isEmpty()) {
                    edit_lname.setError("Enter Last Name");
                } else {
                    changeName();
                }


            }
        });
    }

    void changeName() {
        if (isNetworkAvailable()) {
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final String firstname = edit_fname.getText().toString();
            final String lastname = edit_lname.getText().toString();

            final ProgressDialog progressDialog = new ProgressDialog(Change_Name.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            final StringRequest jsonObjName = new StringRequest(Request.Method.GET, url + "fname=" +
                    firstname + "&lname=" + lastname + "&email=&mobile=&password=&uid=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);

                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonstatus = object.getString("status");

                        switch (Integer.parseInt(jsonstatus)) {
                            case 1:
                                progressDialog.dismiss();
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                editor = sharedpreferences.edit();
                                editor.putString("name", firstname.concat(" " + lastname));

                                editor.putString("fname", firstname);
                                editor.putString("lname", lastname);
                                editor.commit();
                                finish();
                                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                              //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                break;
                            default:
                                progressDialog.dismiss();
                                Toast.makeText(Change_Name.this, "Error " + response, Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Change_Name.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Change_Name.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(jsonObjName);
        } else {
            Toast.makeText(Change_Name.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager name_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = name_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
      //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

}