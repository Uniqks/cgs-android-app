package com.syneotek.dispatch.dispatch.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.syneotek.dispatch.R;

/**
 * Created by Syneotek on 3/15/2016.
 */
public class Create_Profile extends Activity
{
    ImageView img_back_arrow;
    TextView txt_title;
    Button btn_submit;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_profile);

        txt_title = (TextView)findViewById(R.id.toolbar_title);
        txt_title.setText("Create Profile");

        img_back_arrow = (ImageView)findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        btn_submit=(Button)findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                finish();
            }
        });

    }

}


