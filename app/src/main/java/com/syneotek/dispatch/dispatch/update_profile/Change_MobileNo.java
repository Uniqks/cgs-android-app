package com.syneotek.dispatch.dispatch.update_profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Syneotek on 3/16/2016.
 */
public class Change_MobileNo extends Activity {

    private static final String TAG = Change_Name.class.getSimpleName();
    private static final String url = Const.HOSTNAME+"wseditProfile.php?";
    public static String uid = "";
    ImageView img_back_arrow;
    TextView txt_title;
    Button btn_change_mobileno;
    EditText edit_mobile_hint;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_change_mobile);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Change Mobile No");

        edit_mobile_hint = (EditText) findViewById(R.id.edit_mobile_hint);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        edit_mobile_hint.setText(sharedpreferences.getString("mobile", "No Mobile No"));

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);


        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
              //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });


        edit_mobile_hint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                edit_mobile_hint.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edit_mobile_hint.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                edit_mobile_hint.setError(null);
            }
        });

        btn_change_mobileno = (Button) findViewById(R.id.btn_mobileno_change);
        btn_change_mobileno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edit_mobile_hint.getText().toString().trim().isEmpty()) {
                    edit_mobile_hint.setError("Enter Mobile Number");
                }  else {
                    changeMobile();
                }

            }
        });
    }

    void changeMobile() {
        if (isNetworkAvailable()) {
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final String str_mobile = edit_mobile_hint.getText().toString();
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            final ProgressDialog progressDialog = new ProgressDialog(Change_MobileNo.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonObjMobile = new StringRequest(Request.Method.GET, url + "fname=&lname=&email=&mobile="+str_mobile+"&password=&uid=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonstatus = object.getString("status");

                        switch (Integer.parseInt(jsonstatus)) {
                            case 1:
                                progressDialog.dismiss();
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                editor = sharedpreferences.edit();
                                editor.putString("mobile", str_mobile);
                                editor.commit();
                                finish();

                                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                              //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                break;
                            default:
                                progressDialog.dismiss();
                                Toast.makeText(Change_MobileNo.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Change_MobileNo.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Change_MobileNo.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(jsonObjMobile);
        } else {
            Toast.makeText(Change_MobileNo.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager mobile_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = mobile_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
      //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);

        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

}