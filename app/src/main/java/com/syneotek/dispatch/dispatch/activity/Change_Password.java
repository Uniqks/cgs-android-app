package com.syneotek.dispatch.dispatch.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Syneotek on 3/16/2016.
 */
public class Change_Password extends Activity {

    private static final String TAG = Change_Password.class.getSimpleName();
    private static final String url = Const.HOSTNAME + "wseditProfile.php?";
    public static String uid = "";
    ImageView img_back_arrow;
    TextView txt_title;
    EditText edit_oldpassword_hint, edit_newpassword_hint, edit_renewpassword_hint;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    Button btn_update;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_change_password);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Change Password");

        edit_oldpassword_hint = (EditText) findViewById(R.id.edit_oldpassword_hint);
        edit_newpassword_hint = (EditText) findViewById(R.id.edit_newpassword_hint);
        edit_renewpassword_hint = (EditText) findViewById(R.id.edit_renewpassword_hint);

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        btn_update = (Button) findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String str_new_pass = edit_newpassword_hint.getText().toString();
                final String str_con_new_pass = edit_renewpassword_hint.getText().toString();
                if ((str_new_pass.equalsIgnoreCase(str_con_new_pass)) && str_new_pass.trim().length() > 0 && str_con_new_pass.trim().length() > 0) {
                    changePass();
                } else if (str_new_pass.length() == 0) {
                    edit_newpassword_hint.setError("Enter New Pass");
                    Toast.makeText(getApplicationContext(), "Please enter your New Password", Toast.LENGTH_LONG).show();
                } else if (str_con_new_pass.length() == 0) {
                    edit_renewpassword_hint.setError("Re-Enter New Pass");
                    Toast.makeText(getApplicationContext(), "Please re-enter your New Password", Toast.LENGTH_LONG).show();
                } else if (!str_new_pass.equalsIgnoreCase(str_con_new_pass)) {
                    edit_renewpassword_hint.setError("Wrong Pass !");
                    Toast.makeText(getApplicationContext(), "Please enter your Correct New Password", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    void changePass() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        uid = sharedpreferences.getString("uid", "0");
        String sh_pass = sharedpreferences.getString("pass", "");
        final String str_old_pass = edit_oldpassword_hint.getText().toString();
        final String str_new_pass = edit_newpassword_hint.getText().toString();
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final ProgressDialog progressDialog = new ProgressDialog(Change_Password.this);
        if ((str_old_pass.equalsIgnoreCase(sh_pass)) && str_old_pass.trim().length() > 0 && str_new_pass.trim().length() > 0) {
            if (isNetworkAvailable()) {
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                final StringRequest jsonObjPass = new StringRequest(Request.Method.GET, url + "fname=&lname=&email=&mobile=&password=" + str_new_pass + "&uid=" + uid, new Response.Listener<String>() {
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject object = new JSONObject(response);
                            String jsonstatus = object.getString("status");

                            switch (Integer.parseInt(jsonstatus)) {
                                case 1:
                                    progressDialog.dismiss();
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    editor = sharedpreferences.edit();
                                    editor.putString("pass", str_new_pass);
                                    editor.commit();
                                    Toast.makeText(Change_Password.this, "Please re-login with New Password !", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Change_Password.this, Login_Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finishAffinity();
                                    overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                                    break;
                                default:
                                    progressDialog.dismiss();
                                    Toast.makeText(Change_Password.this, "Error", Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(Change_Password.this, "Error while loading data", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(Change_Password.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                    }
                });
                requestQueue.add(jsonObjPass);
            } else {
                Toast.makeText(Change_Password.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else if (str_old_pass.length() == 0) {
            edit_oldpassword_hint.setError("Enter Old Pass");
            Toast.makeText(getApplicationContext(), "Please enter your Old Password", Toast.LENGTH_LONG).show();
        } else if (str_new_pass.length() == 0) {
            edit_newpassword_hint.setError("Enter New Pass");
            Toast.makeText(getApplicationContext(), "Please enter your New Password", Toast.LENGTH_LONG).show();
        } else if (!str_old_pass.equalsIgnoreCase(sh_pass)) {
            edit_oldpassword_hint.setError("Wrong Pass !");
            Toast.makeText(getApplicationContext(), "Please enter your Correct Old Password", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager pass_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = pass_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }
}
