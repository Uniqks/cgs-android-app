package com.syneotek.dispatch.dispatch.feedback_ratingbar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.Main_Activity_Nevigation;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.dispatch.activity.Take_Picture;
import com.syneotek.dispatch.dispatch.signature.Signature_Activity;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Feedback_Activity extends AppCompatActivity {

    private String UPLOAD_URL = Const.HOSTNAME + "wsfinishjob.php?";

    private String KEY_IMAGE = "images";
    private String KEY_RATING = "rating";
    private String KEY_SIGNATURE = "signature";
    String TAG_STATUS_CODE = "status";
    String ratingStars;

    private ListView listView;
    private ArrayAdapter<Item> adapter;
    private ArrayList<Item> arrayList;

    private Bitmap bitmap;

    ImageView img_back_arrow;
    TextView txt_title;

    LinearLayout linear_comment;

    Button btn_submit, btn_cancle;
    RadioGroup radioGroup;
    EditText ed_comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

        linear_comment = (LinearLayout) findViewById(R.id.linear_comment);
//        linear_comment.setVisibility(LinearLayout.GONE);
//        if (!Survay_Details.certificate.equalsIgnoreCase("no")) {
        linear_comment.setVisibility(LinearLayout.VISIBLE);
//        } else {
//            linear_comment.setVisibility(LinearLayout.GONE);
//        }

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
//                    Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        ed_comment = (EditText) findViewById(R.id.ed_comment);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Submit Your Feedback");

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadFeedback();
            }
        });
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_smith = new Intent(Feedback_Activity.this, Main_Activity_Nevigation.class);
                intent_smith.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent_smith);
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                finish();
            }
        });


        listView = (ListView) findViewById(R.id.feedback_listview);
        setLisData();
        adapter = new ListViewAdapter(this, R.layout.feedback_item, arrayList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(onItemClickListener());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

    private void uploadFeedback() {
        try {
            //Showing the progress dialog
            SharedPreferences sharedpreferences = getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            String uid = sharedpreferences.getString("uid", "0");
            RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
            String comment = ed_comment.getText().toString().replace(" ", "%20");
            String feedback = rb.getText().toString();
            ratingStars = "";
            for (int i = 0; i < arrayList.size(); i++) {
                ratingStars = ratingStars.concat(String.valueOf(arrayList.get(i).getRatingStar()).concat(","));
            }
            Log.e("Rating", ratingStars);

//        ((Take_Picture.imgNameArrayList.size() > 0) ? Take_Picture.imgNameArrayList.get(0) : "img1.jpg") + ","+
//                ((Take_Picture.imgNameArrayList.size() > 1) ? Take_Picture.imgNameArrayList.get(1) : "img2.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 2) ? Take_Picture.imgNameArrayList.get(2) : "img3.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 3) ? Take_Picture.imgNameArrayList.get(3) : "img4.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 4) ? Take_Picture.imgNameArrayList.get(4) : "img5.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 5) ? Take_Picture.imgNameArrayList.get(5) : "img6.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 6) ? Take_Picture.imgNameArrayList.get(6) : "img7.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 7) ? Take_Picture.imgNameArrayList.get(7) : "img8.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 8) ? Take_Picture.imgNameArrayList.get(8) : "img9.jpg")+","+
//                ((Take_Picture.imgNameArrayList.size() > 9) ? Take_Picture.imgNameArrayList.get(9) : "img10.jpg")

            RequestQueue requestQueue = Volley.newRequestQueue(this);

            final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringFinishRequest = new StringRequest(Request.Method.GET, UPLOAD_URL
                    + "driver_id=" + uid
                    + "&job_id=" + Survay_Details.job_id
                    + "&images=" + Take_Picture.imageNames.substring(0, Take_Picture.imageNames.length() - 1)
                    + "&rating=" + String.valueOf(arrayList.get(0).getRatingStar())
                    + "&rating_two=" + String.valueOf(arrayList.get(1).getRatingStar())
                    + "&rating_three=" + String.valueOf(arrayList.get(2).getRatingStar())
                    + "&signature=" + Signature_Activity.img_name
                    + "&comment=" + comment
                    + "&status=" + feedback, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString(TAG_STATUS_CODE);
                        switch (Integer.parseInt(data)) {
                            case 1:
//                            Toast.makeText(Feedback_Activity.this, "Image Uploaded !", Toast.LENGTH_LONG).show();
                                loading.dismiss();
                                Intent intent_smith = new Intent(Feedback_Activity.this, Main_Activity_Nevigation.class);
                                intent_smith.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent_smith);
                                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
//                          overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                                finish();

                                break;
                            default:
                                loading.dismiss();
                                Toast.makeText(Feedback_Activity.this, "Error:", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Feedback_Activity.this, "Error: ", Toast.LENGTH_LONG).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            loading.dismiss();
                            Toast.makeText(Feedback_Activity.this, "Error: ", Toast.LENGTH_LONG).show();
                        }
                    });
            stringFinishRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //Creating a Request Queue
            requestQueue.add(stringFinishRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AdapterView.OnItemClickListener onItemClickListener() {
        return new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Dialog dialog = new Dialog(Feedback_Activity.this);
                dialog.setContentView(R.layout.layout_dailog);
                dialog.setTitle("Movie details");

                TextView name = (TextView) dialog.findViewById(R.id.name);
                TextView starRate = (TextView) dialog.findViewById(R.id.rate);

                Item item = (Item) parent.getAdapter().getItem(position);
                name.setText("name: " + item.getName());
                starRate.setText("Yourrate: " + item.getRatingStar());

                dialog.show();
            }
        };
    }

    private void setLisData() {
        arrayList = new ArrayList<>();
        arrayList.add(new Item(0, "Did our installer complete the job ?", ""));
        arrayList.add(new Item(0, "Did our installer clean after the completion ?", ""));
        arrayList.add(new Item(0, "Did our installer explain the details and how it works ?", ""));

    }
}