package com.syneotek.dispatch.dispatch.feedback_ratingbar;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.syneotek.dispatch.R;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Item> {

    private AppCompatActivity activity;
    private List<Item> list;

    public ListViewAdapter(AppCompatActivity context, int resource, List<Item> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.list = objects;
    }

    @Override
    public Item getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.feedback_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
            //holder.ratingBar.getTag(position);
        }

        holder.ratingBar.setOnRatingBarChangeListener(onRatingChangedListener(holder, position));

        holder.ratingBar.setTag(position);
        holder.ratingBar.setRating(getItem(position).getRatingStar());
        holder.Name.setText(getItem(position).getName());

        return convertView;
    }

    private RatingBar.OnRatingBarChangeListener onRatingChangedListener(final ViewHolder holder, final int position) {
        return new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Item item = getItem(position);
                if (item != null) {
                    item.setRatingStar(v);
                    Log.e("Adapter", "star: " + v);
                }
            }
        };
    }

    private static class ViewHolder {
        private RatingBar ratingBar;
        private TextView Name;

        public ViewHolder(View view) {
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
//            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
//            stars.getDrawable(2).setColorFilter(Color.parseColor("#4378B1"), PorterDuff.Mode.SRC_ATOP);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                Drawable progress = ratingBar.getProgressDrawable();
                DrawableCompat.setTint(progress, Color.parseColor("#FFFFFF"));
            }
//            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                Drawable progress = ratingBar.getProgressDrawable();
//                DrawableCompat.setTint(progress, Color.parseColor("#FFFFFF"));
//            }
            Name = (TextView) view.findViewById(R.id.txt_delivery);
        }
    }
}