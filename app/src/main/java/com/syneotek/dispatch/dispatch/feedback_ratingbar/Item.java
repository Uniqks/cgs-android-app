package com.syneotek.dispatch.dispatch.feedback_ratingbar;

public class Item {

    private float ratingStar;
    private String name;


    public Item(int ratingStar, String name, String country) {
        this.ratingStar = ratingStar;
        this.name = name;

    }

    public float getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(float ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

