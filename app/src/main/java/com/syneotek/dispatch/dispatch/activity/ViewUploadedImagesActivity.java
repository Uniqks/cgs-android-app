package com.syneotek.dispatch.dispatch.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.MyPublicButtAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class ViewUploadedImagesActivity extends AppCompatActivity {

    private ArrayList<String> mDataSet;
    MyPublicButtAdapter adapter;
    RecyclerView recyclerView;
    TextView txtNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_uploaded_images);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Uploaded Images");

        txtNoData = (TextView) findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.buttListView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        Bundle bundle = getIntent().getExtras();
        String images = bundle.getString("Images");
        if (images != null) {
            String[] splitImage = images.split(",");
            mDataSet = new ArrayList<>(Arrays.asList(splitImage));
        }
        adapter = new MyPublicButtAdapter(this, mDataSet);
        recyclerView.setAdapter(adapter);

//        getData();
//        new getPublicButt().execute(preferences.getString(Constant.FACEBOOK_ID, ""));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
