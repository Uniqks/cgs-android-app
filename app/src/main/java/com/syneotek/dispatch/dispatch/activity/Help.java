package com.syneotek.dispatch.dispatch.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.Quotation_Msg_Adapter;
import com.syneotek.dispatch.model.HelpMsg;
import com.syneotek.dispatch.utils.Const;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by Syneotek on 01/04/2016.
 */
public class Help extends AppCompatActivity {

    private static final String TAG = Help.class.getSimpleName();
    private static final String url = Const.HOSTNAME + "wssendhelp.php?";
    private static final String url_help_list = Const.HOSTNAME + "wsgetmessage.php?";
    public static String uid = "";
    private ListView listView_help;
    private List<HelpMsg> list_msg = new ArrayList<HelpMsg>();
    private Quotation_Msg_Adapter adapter;
    ImageView img_back_arrow;
    TextView txt_title, txt_submit_help, txt_cancle_help;
    EditText ed_help;
    public static String job_id = "";
    int PICKFILE_REQUEST_CODE = 1;
    private static final String upLoadServerUri = Const.HOSTNAME + "uploadandquote.php";
    private static final String uploaded_file = "image";
    private static String uploaded_file_name = "";
    ImageView txt_add_quo_file;
    private Timer timer;

    //Button btn_mark_as_complaint;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        listView_help = (ListView) findViewById(R.id.list_help);
        adapter = new Quotation_Msg_Adapter(Help.this, list_msg);
        listView_help.setAdapter(adapter);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Help");

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        ed_help = (EditText) findViewById(R.id.ed_help);

        txt_submit_help = (TextView) findViewById(R.id.txt_submit_help);
        txt_submit_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_help.getText().toString().trim().isEmpty()) {
//                    ed_help.setError("Please enter some text !");
                    if (!uploaded_file_name.isEmpty()) {
                        send_help("%20");
                    }
                } else {
                    send_help(ed_help.getText().toString());
                }

            }
        });

        txt_add_quo_file = (ImageView) findViewById(R.id.txt_add_help_file);
        txt_add_quo_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                selectFile();
            }
        });

        ed_help.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_help.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_help.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_help.setError(null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Intent intent = getIntent();
                job_id = intent.getStringExtra("Job_ID");
                loadHelpMsgList();
            }
        }, TimeUnit.SECONDS.toMillis(0), TimeUnit.SECONDS.toMillis(10));

    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timer = null;
    }


    void selectFile() {
        Const.verifyStoragePermissions(Help.this);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", "*/*");
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null) {
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, PICKFILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Help.this, "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }

//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.putExtra("CONTENT_TYPE", "*/*");
//        intent.addCategory(Intent.CATEGORY_DEFAULT);
//        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            String Fpath = data.getDataString();
            Uri selectedImageUri = data.getData();
            Toast.makeText(Help.this, selectedImageUri.getPath(), Toast.LENGTH_SHORT).show();
            uploadFile(Const.getPath(Help.this, selectedImageUri));
        }
        // do somthing...
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void uploadFile(final String path) {
        //Showing the progress dialog
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            final ProgressDialog progressDialog = new ProgressDialog(Help.this);
            progressDialog.setMessage("Uploading...");
            progressDialog.show();
            StringRequest stringFileRequest = new StringRequest(Request.Method.POST, upLoadServerUri, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString("status");
                        switch (Integer.parseInt(data)) {
                            case 1:
                                String info = object.getString("info");
                                uploaded_file_name = info;
                                progressDialog.dismiss();
                                Toast.makeText(Help.this, "File Uploaded !", Toast.LENGTH_SHORT).show();

                                break;
                            default:
                                progressDialog.dismiss();
                                Toast.makeText(Help.this, "Error:", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Help.this, "Error: ", Toast.LENGTH_SHORT).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            progressDialog.dismiss();
                            Toast.makeText(Help.this, "Please Select File ", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    Map<String, String> params = new Hashtable<String, String>();
                    String image = getStringImage(path);
                    params.put(uploaded_file, image);
                    params.put("file", getExtension(path));
                    //returning parameters
                    return params;
                }
            };

            stringFileRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //Creating a Request Queue
//            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//            requestQueue.add(stringQueRequest);
            requestQueue.add(stringFileRequest);
        } else {
            Toast.makeText(Help.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private String getStringImage(String fileName) {
        File originalFile = new File(fileName);
        String encodedBase64 = null;
        try {
            Const.verifyStoragePermissions(Help.this);
            File path = originalFile.getAbsoluteFile();
            FileInputStream fileInputStreamReader = new FileInputStream(path);
            byte[] bytes = new byte[(int) originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));
            Log.e("String File", encodedBase64);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }

    public static String getExtension(String fileName) {
        String encoded;
        try {
            encoded = URLEncoder.encode(fileName, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            encoded = fileName;
        }
        return MimeTypeMap.getFileExtensionFromUrl(encoded).toLowerCase();
    }



    @Override
    protected void onStart() {
        super.onStart();
        hideKeyboard();
//        Intent intent = getIntent();
//        job_id = intent.getStringExtra("Job_ID");
//        loadHelpMsgList();
    }

    void send_help(String msg) {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Help.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_help = new StringRequest(Request.Method.GET, url + "sender_id=" + uid + "&job_id=" + job_id + "&message=" + msg.replace(" ", "%20").replace("\n", "%20") + "&filename=" + uploaded_file_name, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                hideKeyboard();
                                Toast.makeText(Help.this, "Help message send !", Toast.LENGTH_LONG).show();
                                ed_help.getText().clear();
                                uploaded_file_name = "";
                                loadHelpMsgList();
                                break;
                            default:
                                hideKeyboard();
                                Toast.makeText(Help.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Help.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideKeyboard();
                    Toast.makeText(Help.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonReq_help);

        } else {
            Toast.makeText(Help.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void loadHelpMsgList() {
        if (isNetworkAvailable()) {
            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
//            final ProgressDialog progressDialog = new ProgressDialog(Help.this);
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            final StringRequest jsonObjMsgList = new StringRequest(Request.Method.GET, url_help_list + "driver_id=" + uid + "&job_id=" + job_id, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                list_msg.clear();
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    HelpMsg data = new HelpMsg();
                                    data.sender_id = jsonObject.getString("sender_id");
                                    data.sender_type = jsonObject.getString("sender_type");
                                    data.receiever_id = jsonObject.getString("receiever_id");
                                    data.receiever_type = jsonObject.getString("receiver_type");
                                    data.help_message = jsonObject.getString("message");
                                    data.date = jsonObject.getString("date");
                                    data.filename = jsonObject.getString("filename");
                                    list_msg.add(data);

                                }
                                adapter.notifyDataSetChanged();
//                                progressDialog.dismiss();
                                break;
                            default:
//                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Help.this, "Error", Toast.LENGTH_LONG).show();
//                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Help.this, "Error", Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjMsgList);

        } else {
            Toast.makeText(Help.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed_help.getWindowToken(), 0);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager help_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = help_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }
}
