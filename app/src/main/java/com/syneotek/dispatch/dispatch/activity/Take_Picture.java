package com.syneotek.dispatch.dispatch.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.BuildConfig;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.signature.Signature_Activity;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class Take_Picture extends Activity {

    //Request codes

    final int GALLERY = 0;
    final int CAMERA = 1;

    //Context
    Context context;

    //Uri used when picking from the camera
    Uri imageURI;

    final public static String IMAGEFOLDER = "S4Test";
    // private String UPLOAD_URL ="http://syneoteksoft.com/GasAgency/wsfinishjob.php?driver_id=9&job_id=2&images=img1.jpg,img2.jpg,img3.jpg&rating=4.2&signature=sign1.jpg";

    private String UPLOAD_URL = Const.HOSTNAME + "uploadprofileimage.php";
    private String KEY_IMAGE = "image";
    private int PICK_IMAGE_REQUEST = 1;
    String TAG_STATUS_CODE = "status";
    //    private Bitmap bitmap;
    public static ArrayList<Bitmap> bitmapArrayList = new ArrayList<Bitmap>();
    public static ArrayList<String> imgNameArrayList = new ArrayList<String>();
    public static String imageNames = "";

    ImageView img_back_arrow;
    ImageView img_camera;
    ImageView loadto_imageview, loadto_imageview_second, loadto_imageview_third, loadto_imageview_fourth,
            loadto_imageview_fifth, loadto_imageview_sixth, loadto_imageview_seventh, loadto_imageview_eaighth,
            loadto_imageview_nineth, loadto_imageview_tenth;

    TextView txt_title;
    TextView txt_take_picture, txt_image;
    Button btn_next;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_FILE2 = 2;
    Button btnSelect;

    int count = 1;

    public static ArrayList<String> images = new ArrayList<String>();
    ProgressDialog loading;

    String mCurrentPhotoPath;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_images);
        Const.verifyStoragePermissions(Take_Picture.this);

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("Upload Images");

        imgNameArrayList.clear();

        loadto_imageview = (ImageView) findViewById(R.id.img_first);
        loadto_imageview_second = (ImageView) findViewById(R.id.img_second);
        loadto_imageview_third = (ImageView) findViewById(R.id.img_third);
        loadto_imageview_fourth = (ImageView) findViewById(R.id.img_fourth);
        loadto_imageview_fifth = (ImageView) findViewById(R.id.img_fifth);
        loadto_imageview_sixth = (ImageView) findViewById(R.id.img_sixth);
        loadto_imageview_seventh = (ImageView) findViewById(R.id.img_seventh);
        loadto_imageview_eaighth = (ImageView) findViewById(R.id.img_eaight);
        loadto_imageview_nineth = (ImageView) findViewById(R.id.img_nineth);
        loadto_imageview_tenth = (ImageView) findViewById(R.id.img_tenth);
        txt_image = (TextView) findViewById(R.id.txt_image);
        txt_take_picture = (TextView) findViewById(R.id.txt_take_picture);

//        img_camera = (ImageView) findViewById(R.id.img_camera);
//        img_camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                selectImage();
//                //count++;
//
//
//            }
//        });

        txt_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                intent.setType("image/*");
//                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

//                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
//                startActivityForResult(galleryIntent, SELECT_FILE);

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, GALLERY);


            }
        });

        txt_take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(intent, REQUEST_CAMERA);

                Intent getCameraImage = new Intent("android.media.action.IMAGE_CAPTURE");

                File cameraFolder;

                //Check to see if there is an SD card mounted
                if (Environment.getExternalStorageState().equals
                        (Environment.MEDIA_MOUNTED))
                    cameraFolder = new File(Environment.getExternalStorageDirectory(),
                            IMAGEFOLDER);
                else
                    cameraFolder = Take_Picture.this.getCacheDir();
                if (!cameraFolder.exists())
                    cameraFolder.mkdirs();

                //Appending timestamp to "picture_"
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
                String timeStamp = dateFormat.format(new Date());
                String imageFileName = "picture_" + timeStamp + ".jpg";



                //-----------------22-1-2018---------------
                File photo = new File(Environment.getExternalStorageDirectory(),
                        IMAGEFOLDER + "/" + imageFileName);
                imageURI = FileProvider.getUriForFile(Take_Picture.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photo);

                mCurrentPhotoPath = "file:" + photo.getAbsolutePath();

                getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                getCameraImage.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                //  getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                //Setting a global variable to be used in the OnActivityResult
                //imageURI = Uri.fromFile(photo);

                //-------------




                startActivityForResult(getCameraImage, CAMERA);
            }
        });

        loadto_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_fourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_fifth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_sixth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_seventh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_eaighth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_nineth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        loadto_imageview_tenth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
                //count++;


            }
        });

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                // overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 2) {
                    imageNames = "";
                    for (int i = 0; i < imgNameArrayList.size(); i++) {
                        imageNames = imageNames.concat(imgNameArrayList.get(i).concat(","));
                        Log.e("ImageName", imageNames);

                    }
                    Intent intent = new Intent(Take_Picture.this, Signature_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                    // overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                } else {
                    Toast.makeText(Take_Picture.this, "Minimum two images has to upload!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void uploadImage(final Bitmap bitmap) {
        //Showing the progress dialog
        if (isNetworkAvailable()) {

            loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
            StringRequest stringPicRequest = new StringRequest(Request.Method.POST, UPLOAD_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString(TAG_STATUS_CODE);
                        switch (Integer.parseInt(data)) {
                            case 1:
                                String info = object.getString("info");
                                imgNameArrayList.add(info);
                                loading.dismiss();
                                Toast.makeText(Take_Picture.this, "Image Uploaded !", Toast.LENGTH_LONG).show();

//                                if (bitmap != null) {
//                                    bitmap.recycle();
//                                    bitmap = null;
//                                }

                                break;
                            default:
                                loading.dismiss();
                                Toast.makeText(Take_Picture.this, "Error:", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        e.printStackTrace();
                        Toast.makeText(Take_Picture.this, "Error: ", Toast.LENGTH_LONG).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            loading.dismiss();
                            Toast.makeText(Take_Picture.this, "Please Select Image ", Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    String image = getStringImage(bitmap);
                    Map<String, String> params = new Hashtable<String, String>();
                    params.put(KEY_IMAGE, image);

                    //returning parameters
                    return params;
                }
            };

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringPicRequest);
        } else {
            Toast.makeText(Take_Picture.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(Take_Picture.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);

                    //Specify a camera intent
                    Intent getCameraImage = new Intent("android.media.action.IMAGE_CAPTURE");

                    File cameraFolder;

                    //Check to see if there is an SD card mounted
                    if (Environment.getExternalStorageState().equals
                            (Environment.MEDIA_MOUNTED))
                        cameraFolder = new File(Environment.getExternalStorageDirectory(),
                                IMAGEFOLDER);
                    else
                        cameraFolder = Take_Picture.this.getCacheDir();
                    if (!cameraFolder.exists())
                        cameraFolder.mkdirs();

                    //Appending timestamp to "picture_"
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
                    String timeStamp = dateFormat.format(new Date());
                    String imageFileName = "picture_" + timeStamp + ".jpg";

                    //-----------------22-1-2018---------------
                    File photo = new File(Environment.getExternalStorageDirectory(),
                            IMAGEFOLDER + "/" + imageFileName);
                    imageURI = FileProvider.getUriForFile(Take_Picture.this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            photo);

                    mCurrentPhotoPath = "file:" + photo.getAbsolutePath();

                    getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                    getCameraImage.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    //  getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                    //Setting a global variable to be used in the OnActivityResult
                    //imageURI = Uri.fromFile(photo);

                    //-------------


                  /*  File photo = new File(Environment.getExternalStorageDirectory(),
                            IMAGEFOLDER + imageFileName);
                    getCameraImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));

                    //Setting a global variable to be used in the OnActivityResult
                    imageURI = Uri.fromFile(photo);*/

                    startActivityForResult(getCameraImage, CAMERA);

                } else if (items[item].equals("Choose from Gallery")) {

                    //Launching the gallery
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, GALLERY);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == SELECT_FILE) {
//                onSelectFromGalleryResult(data);
//                if (bitmap != null) {
//                    count++;
//                    uploadImage();
//                }
//
//            } else if (requestCode == REQUEST_CAMERA) {
//                onCaptureImageResult(data);
//                if (bitmap != null) {
//                    count++;
//                    uploadImage();
//                }
//
//            }
//
//        }
//    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {


            switch (requestCode) {
                case GALLERY:
                    try {
                        Bitmap bitmap = createScaledBitmap(getImagePath(data, getApplicationContext()), 200, 200);
//                    imageView.setImageBitmap(bitmap);
                        ExifInterface ei = new ExifInterface(getImagePath(data, getApplicationContext()));
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                bitmap = Const.rotateImage(bitmap, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                bitmap = Const.rotateImage(bitmap, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                bitmap = Const.rotateImage(bitmap, 270);
                                break;
                            case ExifInterface.ORIENTATION_NORMAL:
                                break;
                            default:
                                break;
                        }

                        setImageToView(bitmap);
                        count++;
                        uploadImage(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case CAMERA:
                    try {
//                        String path = imageURI.getPath();
                        imageURI = Uri.parse(mCurrentPhotoPath);
                        String path = imageURI.getPath();
                        Bitmap bitmapCamera = createScaledBitmap(path, 200, 200);
                        setImageToView(bitmapCamera);

                        ExifInterface ei = new ExifInterface(path);
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);

                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                bitmapCamera = Const.rotateImage(bitmapCamera, 270);
                                break;
                            case ExifInterface.ORIENTATION_NORMAL:
                                break;
                            default:
                                break;
                        }

                        setImageToView(bitmapCamera);
                        count++;
//                    imageView.setImageBitmap(bitmapCamera);
                        uploadImage(bitmapCamera);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }

    }

    // Function to get image path from ImagePicker
    public static String getImagePath(Intent data, Context context) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }


    public static Bitmap createScaledBitmap(String pathName, int width, int height) {
        final BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, opt);
        opt.inSampleSize = calculateBmpSampleSize(opt, width, height);
        opt.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, opt);
    }

    public static int calculateBmpSampleSize(BitmapFactory.Options opt, int width, int height) {
        final int outHeight = opt.outHeight;
        final int outWidth = opt.outWidth;
        int sampleSize = 1;
        if (outHeight > height || outWidth > width) {
            final int heightRatio = Math.round((float) outHeight / (float) height);
            final int widthRatio = Math.round((float) outWidth / (float) width);
            sampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return sampleSize;
    }

    private void setImageToView(Bitmap bm) {
        if (count == 1) {
            loadto_imageview.setImageBitmap(bm);
        } else if (count == 2) {
            loadto_imageview_second.setImageBitmap(bm);
        } else if (count == 3) {
            loadto_imageview_third.setImageBitmap(bm);
        } else if (count == 4) {
            loadto_imageview_fourth.setImageBitmap(bm);
        } else if (count == 5) {
            loadto_imageview_fifth.setImageBitmap(bm);
        } else if (count == 6) {
            loadto_imageview_sixth.setImageBitmap(bm);
        } else if (count == 7) {
            loadto_imageview_seventh.setImageBitmap(bm);
        } else if (count == 8) {
            loadto_imageview_eaighth.setImageBitmap(bm);
        } else if (count == 9) {
            loadto_imageview_nineth.setImageBitmap(bm);
        } else if (count == 10) {
            loadto_imageview_tenth.setImageBitmap(bm);
        } else
            Toast.makeText(Take_Picture.this, "Maximum ten images will be accpted!", Toast.LENGTH_LONG).show();

    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        loadto_imageview.setImageBitmap(thumbnail);

        if (count == 1) {
            loadto_imageview.setImageBitmap(thumbnail);
        } else if (count == 2) {
            loadto_imageview_second.setImageBitmap(thumbnail);
        } else if (count == 3) {
            loadto_imageview_third.setImageBitmap(thumbnail);
        } else if (count == 4) {
            loadto_imageview_fourth.setImageBitmap(thumbnail);
        } else if (count == 5) {
            loadto_imageview_fifth.setImageBitmap(thumbnail);
        } else if (count == 6) {
            loadto_imageview_sixth.setImageBitmap(thumbnail);
        } else if (count == 7) {
            loadto_imageview_seventh.setImageBitmap(thumbnail);
        } else if (count == 8) {
            loadto_imageview_eaighth.setImageBitmap(thumbnail);
        } else if (count == 9) {
            loadto_imageview_nineth.setImageBitmap(thumbnail);
        } else if (count == 10) {
            loadto_imageview_tenth.setImageBitmap(thumbnail);
        } else
            Toast.makeText(Take_Picture.this, "Maximum ten images will be accpted!", Toast.LENGTH_LONG).show();
//        if (bitmap != null) {
//            // bitmap.recycle();
//            bitmap = null;
//        } else
//        bitmap = thumbnail;
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        if (count == 1) {
            loadto_imageview.setImageBitmap(bm);
        } else if (count == 2) {
            loadto_imageview_second.setImageBitmap(bm);
        } else if (count == 3) {
            loadto_imageview_third.setImageBitmap(bm);
        } else if (count == 4) {
            loadto_imageview_fourth.setImageBitmap(bm);
        } else if (count == 5) {
            loadto_imageview_fifth.setImageBitmap(bm);
        } else if (count == 6) {
            loadto_imageview_sixth.setImageBitmap(bm);
        } else if (count == 7) {
            loadto_imageview_seventh.setImageBitmap(bm);
        } else if (count == 8) {
            loadto_imageview_eaighth.setImageBitmap(bm);
        } else if (count == 9) {
            loadto_imageview_nineth.setImageBitmap(bm);
        } else if (count == 10) {
            loadto_imageview_tenth.setImageBitmap(bm);
        } else
            Toast.makeText(Take_Picture.this, "Maximum ten images will be accpted!", Toast.LENGTH_LONG).show();


//        if (bitmap != null) {
//            //bitmap.recycle();
//            bitmap = null;
//        } else
//        bitmap = bm;
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();

        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
        //  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager pic_cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = pic_cm.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}





