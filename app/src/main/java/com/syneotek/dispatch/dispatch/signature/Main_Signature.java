package com.syneotek.dispatch.dispatch.signature;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.activity.Take_Picture;
import com.syneotek.dispatch.dispatch.feedback_ratingbar.Feedback_Activity;
import com.syneotek.dispatch.utils.Const;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;


public class Main_Signature extends Activity {


    private String UPLOAD_URL = Const.HOSTNAME+"wsfinishjob.php?driver_id=9&job_id=2&images=img1.jpg,img2.jpg,img3.jpg&rating=4.2&signature=sign1.jpg";
    // private String KEY_DRIVER_ID = "driver_id";
    //private String KEY_JOB_ID = "job_id";
    private String KEY_IMAGE = "images";
    // private String KEY_RATING = "rating";
    private String KEY_SIGNATURE = "signature";
    //String TAG_STATUS_CODE="status";

    private Bitmap bitmap;

    TextView txt_title;
    ImageView img_recycle;
    ImageView img_back_arrow;
    Button btn_confirm;
    MyCustomAdapter dataAdapter = null;

    private GestureOverlayView gesture;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature_lay_main);

        txt_title=(TextView)findViewById(R.id.toolbar_title);
        img_recycle=(ImageView)findViewById(R.id.img_recycle);
        img_recycle.setImageResource(R.mipmap.ic_syn);
        txt_title.setText("APP NAME");


        displayListView();


//        ImageView image = (ImageView) findViewById(R.id.converted_img);
//        ByteArrayInputStream imageStreamClient = new ByteArrayInputStream(getIntent().getExtras().getByteArray("draw"));
//        image.setImageBitmap(BitmapFactory.decodeStream(imageStreamClient));


        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main_Signature.this, Take_Picture.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                finish();
            }
        });

        btn_confirm = (Button)findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // uploadSignature();


//                  Intent intent = new Intent(Main_Signature.this, Feedback_Activity.class);
//                  startActivity(intent);
//                  overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
//                  overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);finish();
            }
        });


    }

    private void uploadSignature()
    {
        //Showing the progress dialog
       // final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String s)
            {

                Intent intent = new Intent(Main_Signature.this, Feedback_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                finish();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Toast.makeText(Main_Signature.this, "Please Upload Image ", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);
                Map<String,String> params = new Hashtable<String, String>();
                //params.put(KEY_IMAGE, image);
                params.put(KEY_SIGNATURE, image);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public String getStringImage(Bitmap bmp)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    private void displayListView() {

        //Array list of countries
        ArrayList<Signature_item> list = new ArrayList<Signature_item>();
        Signature_item  list_item = new Signature_item("sdsdfsdfs","",false);
        list.add(list_item);
        list_item = new Signature_item("sddfdsfdfddf","",false);
        list.add(list_item);
        list_item = new Signature_item("sdsdssdsd","",false);
        list.add(list_item);
        list_item = new Signature_item("sdsssdsdsddfrgtd","",false);
        list.add(list_item);

        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(this, R.layout.item_signature, list);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

    }

    private class MyCustomAdapter extends ArrayAdapter<Signature_item> {

        private ArrayList<Signature_item> arrayList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Signature_item> countryList) {
            super(context, textViewResourceId, countryList);
            this.arrayList = new ArrayList<Signature_item>();
            this.arrayList.addAll(countryList);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.item_signature, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.txt_sign);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

//                holder.name.setOnClickListener( new View.OnClickListener() {
//                    public void onClick(View v) {
//                        CheckBox cb = (CheckBox) v ;
//                        Signature_item ckbox = (Signature_item) cb.getTag();
//                        Toast.makeText(getApplicationContext(), "Clicked on Checkbox: " + cb.getText() + " is " + cb.isChecked(), Toast.LENGTH_LONG).show();
//                        ckbox.setSelected(cb.isChecked());
//                    }
//                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Signature_item ck_box = arrayList.get(position);
            holder.code.setText(" (" +  ck_box.getCode() + ")");
            holder.name.setText(ck_box.getName());
            holder.name.setChecked(ck_box.isSelected());
            holder.name.setTag(ck_box);

            return convertView;

        }

    }


}

