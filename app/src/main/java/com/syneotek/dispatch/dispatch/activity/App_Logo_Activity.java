package com.syneotek.dispatch.dispatch.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.syneotek.dispatch.Main_Activity_Nevigation;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.FontChangeCrawler;

/**
 * Created by Syneotek on 3/10/2016.
 */
public class App_Logo_Activity extends AppCompatActivity {
    Button btn_already_login;
    TextView txt_title;

    public static final String MyPREFERENCES = "MyPrefs";

    public static SharedPreferences sharedpreferences, sh;
    public static SharedPreferences.Editor editor, edtr;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_logo);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/OpenSans-Regular.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        txt_title.setText("CGS");

        btn_already_login = (Button) findViewById(R.id.already_sign_up);
        btn_already_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(App_Logo_Activity.this, Login_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains("uid")) {
            Intent intent = new Intent(App_Logo_Activity.this, Main_Activity_Nevigation.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
            finish();
        }
    }
}
