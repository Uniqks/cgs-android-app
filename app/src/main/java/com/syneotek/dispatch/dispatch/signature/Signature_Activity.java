package com.syneotek.dispatch.dispatch.signature;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.feedback_ratingbar.Feedback_Activity;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

public class Signature_Activity extends Activity {

    public static String uid = "";
    private static final String TAG = Signature_Activity.class.getSimpleName();
    private static final String download_url = Const.HOSTNAME + "wsdownloadurl_one.php?";

    String images;
    Integer[] array_btn_id = {R.id.btn_download_one, R.id.btn_download_two, R.id.btn_download_three,
            R.id.btn_download_four, R.id.btn_download_five, R.id.btn_download_six, R.id.btn_download_seven,
            R.id.btn_download_eight};


    TextView txt_title;
    ImageView img_recycle;
    ImageView img_back_arrow;
    Button btn_confirm, showCertificate;
    private String KEY_SIGNATURE = "image";
    private String UPLOAD_URL = Const.HOSTNAME + "uploadprofileimage.php";
    String TAG_STATUS_CODE = "status";
    public static String img_name = "";
//    MyCustomAdapter dataAdapter = null;

    private GestureOverlayView gesture;

    TextView txt_details, txt_address, txt_workingdate;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature_lay);

        //Generate list View from ArrayList
//        displayListView();

        txt_title = (TextView) findViewById(R.id.toolbar_title);
        img_recycle = (ImageView) findViewById(R.id.img_recycle);
        img_recycle.setImageResource(R.mipmap.ic_syn);
        txt_title.setText("Signature");

        txt_details = (TextView) findViewById(R.id.txt_name_sign);
        txt_address = (TextView) findViewById(R.id.txt_address_sign);
        txt_workingdate = (TextView) findViewById(R.id.txt_time_sign);

        txt_details.setText(Survay_Details.details);
        txt_address.setText(Survay_Details.address);
        txt_workingdate.setText(Survay_Details.workingDate);

        img_back_arrow = (ImageView) findViewById(R.id.back_btn);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setImageResource(R.mipmap.image_back);
        img_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        gesture = (GestureOverlayView) findViewById(R.id.signature_box);

        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(Signature_Activity.this, Feedback_Activity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                try {

                    gesture.setDrawingCacheEnabled(true);
                    Bitmap gestureImg = Bitmap.createBitmap(gesture.getDrawingCache());
//                    iv.setImageBitmap(b);
                    gesture.setDrawingCacheEnabled(false);

//                    Bitmap gestureImg = gesture.getGesture().toBitmap(100, 100, 8,Color.WHITE);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    gestureImg.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    byte[] bArray = bos.toByteArray();
                    uploadSignature(gestureImg);

//                    Intent intent = new Intent(Signature_Activity.this, Main_Signature.class);
//                    intent.putExtra("draw", bArray);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
//                    overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Signature_Activity.this, "can not able to convert into image", Toast.LENGTH_SHORT).show();
                }


            }
        });

        showCertificate = (Button) findViewById(R.id.showCertificate);
        showCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDownloadLinks();

               /* Intent intent_smith = new Intent(Signature_Activity.this, Survay_Details.class);
                intent_smith.putExtra("Job_ID", Survay_Details.job_id);
                intent_smith.putExtra("Details", Survay_Details.details);
                intent_smith.putExtra("Address", Survay_Details.address);
                intent_smith.putExtra("Lat", Survay_Details.lat);
                intent_smith.putExtra("Lang", Survay_Details.lango);
                intent_smith.putExtra("Status", "c");
                intent_smith.putExtra("Assigneddate", Survay_Details.ass_date);
                intent_smith.putExtra("Workinddate", Survay_Details.workingDate);
                intent_smith.putExtra("Certificate", Survay_Details.certificate);
                intent_smith.putExtra("Description", Survay_Details.description);
                intent_smith.putExtra("contact", Survay_Details.contact);
                intent_smith.putExtra("from_time", Survay_Details.from_time);
                intent_smith.putExtra("to_time", Survay_Details.to_time);
                intent_smith.putExtra("po", Survay_Details.po);
                intent_smith.putExtra("store", Survay_Details.store);
                intent_smith.putExtra("attachment", Survay_Details.attachment);
                intent_smith.putExtra("price", Survay_Details.price);
                int a = Survay_Details.stype;
                intent_smith.putExtra("stype", Survay_Details.stype);

                startActivity(intent_smith);
//                    progressDialog.dismiss();
                overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);*/
            }
        });
    }

    private void uploadSignature(final Bitmap bitmap) {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringSignRequest = new StringRequest(Request.Method.POST, UPLOAD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("AddImage", response);
                try {
                    JSONObject object = new JSONObject(response);
                    String data = object.getString(TAG_STATUS_CODE);
                    switch (Integer.parseInt(data)) {
                        case 1:
                            img_name = object.getString("info");
                            loading.dismiss();
                            Toast.makeText(Signature_Activity.this, "Signature Uploaded !", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Signature_Activity.this, Feedback_Activity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                            break;
                        default:
                            loading.dismiss();
                            Toast.makeText(Signature_Activity.this, "Error:", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    loading.dismiss();
                    e.printStackTrace();
                    Toast.makeText(Signature_Activity.this, "Error: ", Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(Signature_Activity.this, "Please Do Signature", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String image = getStringImage(bitmap);
                Map<String, String> params = new Hashtable<String, String>();
                params.put(KEY_SIGNATURE, image);
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringSignRequest);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
    }

    private void displayListView() {

//        //Array list of countries
//        ArrayList<Signature_item> list = new ArrayList<Signature_item>();
//        Signature_item list_item = new Signature_item("sdsdfsdfs", "", false);
//        list.add(list_item);
//        list_item = new Signature_item("sddfdsfdfddf", "", false);
//        list.add(list_item);
//        list_item = new Signature_item("sdsdssdsd", "", false);
//        list.add(list_item);
//        list_item = new Signature_item("sdsssdsdsddfrgtd", "", false);
//        list.add(list_item);
//
//        //create an ArrayAdaptar from the String Array
//        dataAdapter = new MyCustomAdapter(this, R.layout.item_signature, list);
//        ListView listView = (ListView) findViewById(R.id.listView1);
//        // Assign adapter to ListView
//        listView.setAdapter(dataAdapter);

    }

//    private class MyCustomAdapter extends ArrayAdapter<Signature_item> {
//
//        private ArrayList<Signature_item> arrayList;
//
//        public MyCustomAdapter(Context context, int textViewResourceId,
//                               ArrayList<Signature_item> countryList) {
//            super(context, textViewResourceId, countryList);
//            this.arrayList = new ArrayList<Signature_item>();
//            this.arrayList.addAll(countryList);
//        }
//
//        private class ViewHolder {
//            TextView code;
//            CheckBox name;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
//            ViewHolder holder = null;
//            Log.v("ConvertView", String.valueOf(position));
//
//            if (convertView == null) {
//                LayoutInflater vi = (LayoutInflater) getSystemService(
//                        Context.LAYOUT_INFLATER_SERVICE);
//                convertView = vi.inflate(R.layout.item_signature, null);
//
//                holder = new ViewHolder();
//                holder.code = (TextView) convertView.findViewById(R.id.txt_sign);
//                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
//                convertView.setTag(holder);
//
////                holder.name.setOnClickListener( new View.OnClickListener() {
////                    public void onClick(View v) {
////                        CheckBox cb = (CheckBox) v ;
////                        Signature_item ckbox = (Signature_item) cb.getTag();
////                        Toast.makeText(getApplicationContext(), "Clicked on Checkbox: " + cb.getText() + " is " + cb.isChecked(), Toast.LENGTH_LONG).show();
////                        ckbox.setSelected(cb.isChecked());
////                    }
////                });
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
//
//            Signature_item ck_box = arrayList.get(position);
//            holder.code.setText(" (" + ck_box.getCode() + ")");
//            holder.name.setText(ck_box.getName());
//            holder.name.setChecked(ck_box.isSelected());
//            holder.name.setTag(ck_box);
//
//            return convertView;
//
//        }
//
//    }

    void showAddExtraDialog(JSONObject object) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Signature_Activity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_show_certificate, null);
        dialogBuilder.setView(dialog);
        dialogBuilder.setCancelable(true);

//        final Dialog dialog = new Dialog(Signature_Activity.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_show_certificate);
//        dialog.setCancelable(true);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);

        JSONArray array = null;
        try {
            array = object.getJSONArray("info");
//            images = object.getString("image");
            for (int i = 0; i < array.length(); i++) {
                final JSONObject object1 = array.getJSONObject(i);
                if (i < array_btn_id.length) {
                    Button button = (Button) dialog.findViewById(array_btn_id[i]);
                    if (button != null) {
                        if (!object1.getString("invoice_type").equalsIgnoreCase("")) {
                            button.setVisibility(Button.VISIBLE);
                            button.setText(object1.getString("invoice_type"));
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String url = null;
                                    try {
                                        url = object1.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);
                                    b.dismiss();
                                }
                            });
                        } else {
                            button.setVisibility(Button.GONE);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

    }


    void getDownloadLinks() {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            String uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(Signature_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_link = new StringRequest(Request.Method.GET, download_url + "driver_id=" + uid + "&job_id=" + Survay_Details.job_id, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    Log.e("Links", response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");
                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:

                                JSONArray array = object.getJSONArray("info");
                                if (array.length() > 0)
                                    showAddExtraDialog(object);
                                else
                                    Toast.makeText(Signature_Activity.this, "No data found please try again!", Toast.LENGTH_LONG).show();

                                break;
                            default:
                                Toast.makeText(Signature_Activity.this, "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(Signature_Activity.this, "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(Signature_Activity.this, "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonReq_link);

        } else {
            Toast.makeText(Signature_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager sur_no = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = sur_no
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}

