package com.syneotek.dispatch.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.syneotek.dispatch.dispatch.activity.Change_Password;
import com.syneotek.dispatch.dispatch.activity.Create_Profile;
import com.syneotek.dispatch.dispatch.activity.Edit_Profile;
import com.syneotek.dispatch.R;

/**
 * Created by Syneotek on 3/10/2016.
 */
public class Setting_Frag extends android.support.v4.app.Fragment
{
    TextView txt_create_profile,txt_editprofile,txt_change_password;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.setting, container, false);

        txt_create_profile=(TextView)v.findViewById(R.id.txt_create_profile);
        txt_create_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Intent intent=new Intent(getActivity(),Create_Profile.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                //getActivity().finish();
            }
        });

        txt_editprofile=(TextView)v.findViewById(R.id.txt_edit_profile);
        txt_editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Intent intent=new Intent(getActivity(),Edit_Profile.class);
                startActivity(intent);
             //   getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                //getActivity().finish();
            }
        });


        txt_change_password=(TextView)v.findViewById(R.id.txt_change_password);
        txt_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Intent intent=new Intent(getActivity(),Change_Password.class);
                startActivity(intent);
               // getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                //getActivity().finish();
            }
        });

        return v;

    }
        public static Setting_Frag newInstance(String text)
        {
            Setting_Frag f = new Setting_Frag();
            Bundle b = new Bundle();
            b.putString("msg", text);
            f.setArguments(b);
            return f;
        }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

           // getActivity.finish();

//            Intent intent=new Intent(getActivity(),Main_Activity_Nevigation.class);
//            startActivity(intent);
//            getActivity().finish();

//            FragmentManager fm = getFragmentManager();
//            FragmentTransaction fragmentTransaction = fm.beginTransaction();
//
//            fragmentTransaction.replace(R.id.main_screen_framelayout, new App_Name_Frag1());
//            fragmentTransaction.addToBackStack(null);
//
//            fragmentTransaction.commit();
//            getActivity().finish();
//            return true;
        }

        return getActivity().onKeyDown(keyCode, event);
    }



    }



