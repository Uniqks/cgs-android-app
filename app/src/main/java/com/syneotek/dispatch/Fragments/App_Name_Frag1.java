package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.CustomListAdapter;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Syneotek on 3/10/2016.
 */
public class App_Name_Frag1 extends Fragment {

    private static final String TAG = App_Name_Frag1.class.getSimpleName();

    private static final String url = Const.HOSTNAME + "wsgetjob.php?";
    private ProgressDialog pDialog;
    private List<app_name_items> appNameItemse_list = new ArrayList<app_name_items>();
    private ListView listView;
    private TextView txt_no_data;
    private CustomListAdapter adapter;
    private TextView txt_current_date;
    public static String cur_date = "";
    public static String uid = "";
    private ImageView img_previous, img_next;
    public static int dateCount = 0;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.app_name, container, false);
        listView = (ListView) v.findViewById(R.id.list);
        txt_no_data = (TextView) v.findViewById(R.id.no_data);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        cur_date = df.format(new Date());
//        editor.putString("cur_date", cur_date);
//        editor.commit();

        adapter = new CustomListAdapter(getActivity(), appNameItemse_list);
        listView.setAdapter(adapter);
//        getListData(cur_date);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setMessage("Loading...");
//                progressDialog.show();
                app_name_items item = new app_name_items();
                item = appNameItemse_list.get(position);


                Intent intent_smith = new Intent(getActivity(), Survay_Details.class);
                intent_smith.putExtra("Job_ID", item.getJID());
                intent_smith.putExtra("Details", item.getDetails());
                intent_smith.putExtra("Address", item.getAddress());
                intent_smith.putExtra("Lat", item.getLat());
                intent_smith.putExtra("Lang", item.getLango());
                intent_smith.putExtra("Status", item.getStatus());
                intent_smith.putExtra("Assigneddate", item.getADATE());
                intent_smith.putExtra("Workinddate", item.getWorkingdate());
                intent_smith.putExtra("Certificate", item.getCert());
                intent_smith.putExtra("Description", item.getDesc());
                intent_smith.putExtra("contact", item.getcontact());
                intent_smith.putExtra("from_time", item.getfrom_time());
                intent_smith.putExtra("to_time", item.getto_time());
                intent_smith.putExtra("po", item.getpo());
                intent_smith.putExtra("store", item.getstore());
                intent_smith.putExtra("attachment", item.getAttachment());
                intent_smith.putExtra("price", item.getPrice());
                int a = item.getStype();
                intent_smith.putExtra("stype", item.getStype());

                intent_smith.putExtra("olat", item.getOlat());
                intent_smith.putExtra("olng", item.getOlng());

                intent_smith.putExtra("hlat", item.getHlat());
                intent_smith.putExtra("hlng", item.getHlng());

                Log.d("O Test", item.getOlat() + " " + item.getOlng());

                Log.e("ITEM----->", item.getStatus() + "");

                startActivity(intent_smith);
//                    progressDialog.dismiss();
                getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);

            }
        });

        txt_current_date = (TextView) v.findViewById(R.id.current_date);
        txt_current_date.setText(sharedpreferences.getString("cur_date", cur_date));
        img_previous = (ImageView) v.findViewById(R.id.previous_date);
        img_next = (ImageView) v.findViewById(R.id.next_date);
        img_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount--;
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, dateCount);
                Date dateBeforeDays = cal.getTime();
                cur_date = df.format(dateBeforeDays);
                editor = sharedpreferences.edit();
                editor.putString("cur_date", cur_date);
                editor.commit();
                txt_current_date.setText(sharedpreferences.getString("cur_date", cur_date));
                getListData(sharedpreferences.getString("cur_date", cur_date));
            }
        });
        img_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount++;
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, dateCount);
                Date dateBeforeDays = cal.getTime();
                cur_date = df.format(dateBeforeDays);
                editor = sharedpreferences.edit();
                editor.putString("cur_date", cur_date);
                editor.commit();
                txt_current_date.setText(sharedpreferences.getString("cur_date", cur_date));
                getListData(sharedpreferences.getString("cur_date", cur_date));
            }
        });

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getListData(sharedpreferences.getString("cur_date", cur_date));
    }

    void getListData(String current_date) {
        if (isNetworkAvailable()) {

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            appNameItemse_list.clear();
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonObjApp = new StringRequest(Request.Method.GET, url + "driver_id=" + uid + "&date=" + current_date, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    app_name_items data = new app_name_items();
                                    data.setJID(jsonObject.getString("job_id"));
                                    data.setDetails(jsonObject.getString("detail"));
                                    data.setAddress(jsonObject.getString("address"));
                                    data.setLat(jsonObject.getString("lat"));
                                    data.setLango(jsonObject.getString("lang"));
                                    data.setStatus(jsonObject.getString("status"));
                                    data.setADATE(jsonObject.getString("assigned_date"));
                                    data.setWorkinddate(jsonObject.getString("working_date"));
                                    data.setCert(jsonObject.getString("certificate"));
                                    data.setDesc(jsonObject.getString("description"));
                                    data.setcontact(jsonObject.getString("contact"));
                                    data.setfrom_time(jsonObject.getString("from_time"));
                                    data.setto_time(jsonObject.getString("to_time"));
                                    data.setpo(jsonObject.getString("po"));
                                    data.setstore(jsonObject.getString("store"));
                                    data.setAttachment(jsonObject.getString("file_name"));
                                    data.setPrice(jsonObject.getString("amount"));
                                    data.setStype(jsonObject.getInt("stype"));

                                    data.setOlat(jsonObject.getString("olat"));
                                    data.setOlng(jsonObject.getString("olng"));

                                    data.setHlat(jsonObject.getString("hlat"));
                                    data.setHlng(jsonObject.getString("hlng"));

                                    Log.d("225 Item Data", data.toString());

                                    appNameItemse_list.add(data);

                                }
                                adapter.notifyDataSetChanged();
                                listView.setVisibility(ListView.VISIBLE);
                                txt_no_data.setVisibility(TextView.GONE);
                                progressDialog.dismiss();
                                break;
                            default:
                                txt_no_data.setVisibility(TextView.VISIBLE);
                                listView.setVisibility(ListView.GONE);
                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                        txt_no_data.setVisibility(TextView.VISIBLE);
                        listView.setVisibility(ListView.GONE);
                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    txt_no_data.setVisibility(TextView.VISIBLE);
                    listView.setVisibility(ListView.GONE);
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjApp);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager app_name_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = app_name_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static App_Name_Frag1 newInstance(String text) {
        App_Name_Frag1 f = new App_Name_Frag1();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

}
