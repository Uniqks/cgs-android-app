package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.Quotation_Msg_Adapter;
import com.syneotek.dispatch.model.HelpMsg;
import com.syneotek.dispatch.utils.Const;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by Syneotek on 13/06/2016.
 */
public class ManageQuotation extends Fragment {

    private static final String TAG = "ManageQuotation";
    private static final String getQuotation = Const.HOSTNAME + "wsgetquotation.php?";
    private static final String add_quotation = Const.HOSTNAME + "wsquotation.php?";
    public static String uid = "";
    ListView listView_quo;
    private List<HelpMsg> list_msg = new ArrayList<HelpMsg>();
    private Quotation_Msg_Adapter adapter;
    TextView txt_quo_submit;
    ImageView txt_add_quo_file;
    EditText ed_add_txt;
    int PICKFILE_REQUEST_CODE = 1;
    private static final String upLoadServerUri = Const.HOSTNAME + "uploadandquote.php";
    private static final String uploaded_file = "image";
    private static String uploaded_file_name = "";
    private Timer timer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.manage_quotation, container, false);
        Const.verifyStoragePermissions(getActivity());
        listView_quo = (ListView) v.findViewById(R.id.listView_quo);
        adapter = new Quotation_Msg_Adapter(getActivity(), list_msg);
        listView_quo.setAdapter(adapter);

        ed_add_txt = (EditText) v.findViewById(R.id.ed_add_txt);

        txt_quo_submit = (TextView) v.findViewById(R.id.txt_quo_submit);
        txt_quo_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_add_txt.getText().toString().trim().isEmpty()) {
//                    ed_add_txt.setError("Please enter some text !");
                    if (!uploaded_file_name.isEmpty()) {
                        send_quo("%20");
                    }
                } else {
                    send_quo(ed_add_txt.getText().toString());
                }
            }
        });

        txt_add_quo_file = (ImageView) v.findViewById(R.id.txt_add_quo_file);
        txt_add_quo_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                selectFile();
            }
        });

        ed_add_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_add_txt.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_add_txt.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_add_txt.setError(null);
            }
        });
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                loadQuotationMsgList();
            }
        }, TimeUnit.SECONDS.toMillis(1), TimeUnit.SECONDS.toMillis(10));

    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timer = null;
    }


    void selectFile() {
        Const.verifyStoragePermissions(getActivity());
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", "*/*");
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getActivity().getPackageManager().resolveActivity(sIntent, 0) != null) {
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, PICKFILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }

//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.putExtra("CONTENT_TYPE", "*/*");
//        intent.addCategory(Intent.CATEGORY_DEFAULT);
//        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            String Fpath = data.getDataString();
            Uri selectedImageUri = data.getData();
            Toast.makeText(getActivity(), selectedImageUri.getPath(), Toast.LENGTH_SHORT).show();
            uploadFile(Const.getPath(getActivity(), selectedImageUri));
        }
        // do somthing...
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void uploadFile(final String path) {
        //Showing the progress dialog
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading...");
            progressDialog.show();
            StringRequest stringFileRequest = new StringRequest(Request.Method.POST, upLoadServerUri, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("AddImage", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String data = object.getString("status");
                        switch (Integer.parseInt(data)) {
                            case 1:
                                String info = object.getString("info");
                                uploaded_file_name = info;
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "File Uploaded !", Toast.LENGTH_SHORT).show();

                                break;
                            default:
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Error:", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error: ", Toast.LENGTH_SHORT).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Please Select File ", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Converting Bitmap to String
                    Map<String, String> params = new Hashtable<String, String>();
                    String image = getStringImage(path);
                    params.put(uploaded_file, image);
                    params.put("file", getExtension(path));
                    //returning parameters
                    return params;
                }
            };

            stringFileRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            //Creating a Request Queue
//            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//            requestQueue.add(stringQueRequest);
            requestQueue.add(stringFileRequest);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private String getStringImage(String fileName) {
        File originalFile = new File(fileName);
        String encodedBase64 = null;
        try {
            Const.verifyStoragePermissions(getActivity());
            File path = originalFile.getAbsoluteFile();
            FileInputStream fileInputStreamReader = new FileInputStream(path);
            byte[] bytes = new byte[(int) originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));
            Log.e("String File", encodedBase64);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }

    public static String getExtension(String fileName) {
        String encoded;
        try {
            encoded = URLEncoder.encode(fileName, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            encoded = fileName;
        }
        return MimeTypeMap.getFileExtensionFromUrl(encoded).toLowerCase();
    }

    @Override
    public void onStart() {
        super.onStart();
        hideKeyboard();
//        loadQuotationMsgList();
    }

    void send_quo(String msg) {
        if (isNetworkAvailable()) {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            final StringRequest jsonReq_help_hf = new StringRequest(Request.Method.GET, add_quotation + "sender_id=" + uid + "&message=" + msg.replace(" ", "%20").replace("\n", "%20") + "&file=" + uploaded_file_name, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Quotation message send !", Toast.LENGTH_SHORT).show();
                                ed_add_txt.getText().clear();
                                uploaded_file_name = "";
                                loadQuotationMsgList();
                                break;
                            default:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideKeyboard();
                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonReq_help_hf);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void loadQuotationMsgList() {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
//            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
            final StringRequest jsonObjMsgList_hf = new StringRequest(Request.Method.GET, getQuotation + "driver_id=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                list_msg.clear();
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    HelpMsg data = new HelpMsg();
                                    data.sender_id = jsonObject.getString("sender_id");
                                    data.sender_type = jsonObject.getString("sender_type");
                                    data.receiever_id = jsonObject.getString("receiever_id");
                                    data.receiever_type = jsonObject.getString("receiver_type");
                                    data.help_message = jsonObject.getString("message");
                                    data.date = jsonObject.getString("date");
                                    data.filename = jsonObject.getString("filename");
                                    list_msg.add(data);

                                }
                                adapter.notifyDataSetChanged();
//                                progressDialog.dismiss();
                                break;
                            default:
//                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error " + error.toString(), Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjMsgList_hf);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed_add_txt.getWindowToken(), 0);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager help_cm_hf = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = help_cm_hf
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
