package com.syneotek.dispatch.Fragments;

public class app_name_items {



    private String job_id;
    private String details;
    private String address;
    private String lat;
    private String lang;
    private String status;
    private String assigned_date;
    private String workinddate;
    private String certificate;
    private String description;
    private String contact;
    private String from_time;
    private String to_time;
    private String po;
    private String store;
    private String attachment;



    private String price;
    private String extaCost;
    private String comment;
    private String image_attachment;
    private int stype;

    public String getOlat() {
        return olat;
    }

    public void setOlat(String olat) {
        this.olat = olat;
    }

    public String getOlng() {
        return olng;
    }

    public void setOlng(String olng) {
        this.olng = olng;
    }

    public String getHlat() {
        return hlat;
    }

    public void setHlat(String hlat) {
        this.hlat = hlat;
    }

    public String getHlng() {
        return hlng;
    }

    public void setHlng(String hlng) {
        this.hlng = hlng;
    }

    private String olat;
    private String olng;

    @Override
    public String toString() {
        return "o test { " +
                "olat='" + olat + '\'' +
                ", olng='" + olng + '\'' +
                ", hlat='" + hlat + '\'' +
                ", hlng='" + hlng + '\'' +
                '}';
    }

    private String hlat;
    private String hlng;

    public String getJID() {
        return job_id;
    }

    public void setJID(String JID) {
        this.job_id = JID;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }


    public String getWorkingdate() {
        return workinddate;
    }

    public void setWorkinddate(String workinddate) {
        this.workinddate = workinddate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLango() {
        return lang;
    }

    public void setLango(String lang) {
        this.lang = lang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getADATE() {
        return assigned_date;
    }

    public void setADATE(String ADATE) {
        this.assigned_date = ADATE;
    }

    public String getCert() {
        return certificate;
    }

    public void setCert(String Certificate) {
        this.certificate = Certificate;
    }

    public String getDesc() {
        return description;
    }

    public void setDesc(String description) {
        this.description = description;
    }

    public String getcontact() {
        return contact;
    }

    public void setcontact(String contact) {
        this.contact = contact;
    }

    public String getfrom_time() {
        return from_time;
    }

    public void setfrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getto_time() {
        return to_time;
    }

    public void setto_time(String to_time) {
        this.to_time = to_time;
    }

    public String getpo() {
        return po;
    }

    public void setpo(String po) {
        this.po = po;
    }

    public String getstore() {
        return store;
    }

    public void setstore(String store) {
        this.store = store;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getExtaCost() {
        return extaCost;
    }

    public void setExtaCost(String extaCost) {
        this.extaCost = extaCost;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getimage_attachment() {
        return image_attachment;
    }

    public void setimage_attachment(String image_attachment) {
        this.image_attachment = image_attachment;
    }

    public int getStype() {
        return stype;
    }

    public void setStype(int stype) {
        this.stype = stype;
    }

}