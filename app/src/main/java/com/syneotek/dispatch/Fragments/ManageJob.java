package com.syneotek.dispatch.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.syneotek.dispatch.Main_Activity_Nevigation;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.utils.Const;
import com.syneotek.dispatch.utils.SelectDateFragment;
import com.syneotek.dispatch.utils.SimpleTimeFragment;
import com.syneotek.dispatch.utils.SimpleToTimeFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Syneotek on 20/05/2016.
 */
public class ManageJob extends Fragment {

    private static final String TAG = "ManageJob";
    private static final String url = Const.HOSTNAME + "wsaddjob.php?";
    public static String uid = "";
    TextView txt_submit_mj, txt_sel_cat, ed_address, txt_sel_job_label, txt_sel_invoice_type;
    public static TextView txt_from_date, txt_from_time, txt_to_time;
    EditText ed_cust_name, ed_job_name, ed_po, ed_store, ed_amount, ed_contact_number, ed_job_desc;
    public static int category = 0;
    public static String job_label = "";
    public static String invoice_type = "", str_invoice_text = "", str_invoice = "";
    int PLACE_PICKER_REQUEST = 1;
    boolean[] is_checked;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.manage_job, container, false);
        Const.verifyMapPermissions(getActivity());
        ed_cust_name = (EditText) v.findViewById(R.id.ed_cust_name);
        ed_job_name = (EditText) v.findViewById(R.id.ed_job_name);
        ed_address = (TextView) v.findViewById(R.id.ed_address);
        txt_sel_job_label = (TextView) v.findViewById(R.id.txt_sel_job_label);
        txt_sel_invoice_type = (TextView) v.findViewById(R.id.txt_sel_invoice_type);
        ed_po = (EditText) v.findViewById(R.id.ed_po);
        ed_store = (EditText) v.findViewById(R.id.ed_store);
        ed_amount = (EditText) v.findViewById(R.id.ed_amount);
        ed_contact_number = (EditText) v.findViewById(R.id.ed_contact_number);
        ed_job_desc = (EditText) v.findViewById(R.id.ed_job_desc);

        txt_submit_mj = (TextView) v.findViewById(R.id.txt_submit_manage_job);

        txt_sel_cat = (TextView) v.findViewById(R.id.txt_sel_category);
        txt_sel_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_sel_cat.setError(null);
                PopupMenu popup = new PopupMenu(getActivity(), v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.category, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
//                        if (item.getTitle().toString().equalsIgnoreCase("Pay System")) {
//                            category = 3;
//                            txt_sel_cat.setText("Pay System");
//                        }

                        if (item.getTitle().toString().equalsIgnoreCase("Additionals")) {
                            category = 2;
                            txt_sel_cat.setText("Additionals");
                        }

                        return true;
                    }
                });
            }
        });

        txt_sel_job_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_sel_job_label.setError(null);
                PopupMenu popup = new PopupMenu(getActivity(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.job_label, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().toString().equalsIgnoreCase("PO Blue")) {
                            job_label = "B";
                            txt_sel_job_label.setText("PO Blue");
                        }

                        if (item.getTitle().toString().equalsIgnoreCase("NO PO Purple")) {
                            job_label = "P";
                            txt_sel_job_label.setText("NO PO Purple");
                        }

                        if (item.getTitle().toString().equalsIgnoreCase("Pending Yellow")) {
                            job_label = "Y";
                            txt_sel_job_label.setText("Pending Yellow");
                        }

                        return true;
                    }
                });
            }
        });

        txt_sel_invoice_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] dialogList = {"GOEMANS INCIDENT", "TASCO INCIDENT", "LOWES INSTALLER INCIDENT",
                        "CGS INSTALLER INCIDENT", "Canadian Gas Service", "CERTIFICATE OF COMPLETION", "GOEMANS JOB COMPLETION CERTIFICATE",
                        "TASCO JOB COMPLETION CERTIFICATE", "GOEMANS SITE Inspection Installer Form", "Tasco SITE Inspection Installer Form"};
                final android.app.AlertDialog.Builder builderDialog = new android.app.AlertDialog.Builder(getActivity());
                builderDialog.setTitle("Select Invoice Type");
                int count = dialogList.length;
                final boolean[] is_checked = new boolean[count];

                final ArrayList<Integer> arrayList = new ArrayList<Integer>();

                str_invoice_text = "";
                str_invoice = "";


                // Creating multiple selection by using setMutliChoiceItem method
                builderDialog.setMultiChoiceItems(dialogList, is_checked,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
                                if (isChecked) {
                                    try {
                                        arrayList.add(whichButton + 1);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//                                    if (str_invoice_text.trim().isEmpty()) {
//                                        str_invoice_text = str_invoice_text + (dialogList[whichButton].toString());
//                                        str_invoice = str_invoice + (String.valueOf(whichButton));
//                                    } else if (str_invoice_text.trim().length() > 0) {
//                                        str_invoice_text = str_invoice_text + ("," + dialogList[whichButton].toString());
//                                        str_invoice = str_invoice + ("," + String.valueOf(whichButton));
//                                    }
                                } else {
                                    try {
                                        arrayList.remove(whichButton);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

//                                    if (str_invoice_text.split(",").length == 0 && !str_invoice_text.trim().isEmpty()) {
//                                        str_invoice_text = str_invoice_text.replace(dialogList[whichButton].toString(), "");
//                                        str_invoice = str_invoice.replace(String.valueOf(whichButton), "");
//                                    } else if (str_invoice_text.split(",").length >= 1 && !str_invoice_text.trim().isEmpty()) {
//                                        str_invoice_text = str_invoice_text.replace("," + dialogList[whichButton].toString(), "");
//                                        str_invoice = str_invoice.replace("," + String.valueOf(whichButton), "");
//                                    }
                                }

                                Log.e("Item ", dialogList[whichButton].toString() + " = " + String.valueOf(isChecked));
                            }
                        });

                builderDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                ListView list = ((android.app.AlertDialog) dialog).getListView();
                                //ListView has boolean array like {1=true, 3=true}, that shows checked items
                                if (arrayList.size() == 0) {
                                    txt_sel_invoice_type.setText("Select invoice type");
                                } else {
                                    for (int i = 0; i < arrayList.size(); i++) {
                                        if (i == 0) {
                                            str_invoice_text = str_invoice_text.concat(dialogList[arrayList.get(i)].toString());
                                            str_invoice = str_invoice.concat(String.valueOf(arrayList.get(i)));
                                        } else if (i == arrayList.size() - 1) {
                                            str_invoice_text = str_invoice_text.concat("," + dialogList[arrayList.get(i)].toString());
                                            str_invoice = str_invoice.concat("," + String.valueOf(arrayList.get(i)));
                                        } else {
                                            str_invoice_text = str_invoice_text.concat("," + dialogList[arrayList.get(i)].toString());
                                            str_invoice = str_invoice.concat("," + String.valueOf(arrayList.get(i)));
                                        }
                                    }
                                    txt_sel_invoice_type.setText(str_invoice_text);
                                }
                                Log.e("str_invoice_text", str_invoice_text);
                                Log.e("str_invoice", str_invoice);
                                dialog.dismiss();
                            }
                        });

                builderDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                android.app.AlertDialog alert = builderDialog.create();
                alert.show();
            }
        });

        txt_from_date = (TextView) v.findViewById(R.id.txt_from_date);
        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_from_date.setError(null);
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        txt_from_time = (TextView) v.findViewById(R.id.txt_from_time);
        txt_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_from_time.setError(null);
                DialogFragment newFragment = new SimpleTimeFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        txt_to_time = (TextView) v.findViewById(R.id.txt_to_time);
        txt_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_to_time.setError(null);
                DialogFragment newFragment = new SimpleToTimeFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        ed_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    ed_address.setError(null);
                    Intent intent = builder.build(getActivity());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        txt_submit_mj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_cust_name.getText().toString().trim().isEmpty()) {
                    ed_cust_name.setError("Enter Customer Name");
                } else if (ed_job_name.getText().toString().trim().isEmpty()) {
                    ed_job_name.setError("Enter Job Name");
                } else if (ed_address.getText().toString().equalsIgnoreCase("pick your address")) {
                    ed_address.setError("Pick your Address");
                    Toast.makeText(getActivity(), "Please pick you address !", Toast.LENGTH_LONG).show();
                    hideKeyboard();
                } else if (ed_po.getText().toString().trim().isEmpty()) {
                    ed_po.setError("Enter P.O.");
                } else if (ed_store.getText().toString().trim().isEmpty()) {
                    ed_store.setError("Enter Store name");
                } else if (ed_contact_number.getText().toString().trim().isEmpty()) {
                    ed_contact_number.setError("Enter Contact no.");
                } else if (ed_job_desc.getText().toString().trim().isEmpty()) {
                    ed_job_desc.setError("Enter Job Description");
                } else if (ed_amount.getText().toString().trim().isEmpty()) {
                    ed_amount.setError("Enter Amount");
                } else if (txt_sel_cat.getText().toString().equalsIgnoreCase("Select Category")) {
                    txt_sel_cat.setError("Please select category");
                } else if (txt_from_date.getText().toString().equalsIgnoreCase("Date")) {
                    txt_from_date.setError("Please select date");
                } else if (txt_from_time.getText().toString().equalsIgnoreCase("From Time")) {
                    txt_from_time.setError("Please select from time");
                } else if (txt_to_time.getText().toString().equalsIgnoreCase("To Time")) {
                    txt_to_time.setError("Please select to time");
                } else if (txt_sel_invoice_type.getText().toString().equalsIgnoreCase("Select Invoice Type")) {
                    txt_sel_invoice_type.setError("Please select invoice type");
                } else if (txt_sel_job_label.getText().toString().equalsIgnoreCase("Select Job Label")) {
                    txt_sel_job_label.setError("Please select job label");
                } else {
                    send_help();
                }
            }
        });

        ed_cust_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_cust_name.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_cust_name.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_cust_name.setError(null);
            }
        });

        ed_job_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_job_name.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_job_name.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_job_name.setError(null);
            }
        });

        ed_po.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_po.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_po.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_po.setError(null);
            }
        });

        ed_store.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_store.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_store.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_store.setError(null);
            }
        });

        ed_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_amount.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_amount.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_amount.setError(null);
            }
        });
        return v;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Context context = getActivity().getApplicationContext();
                Place place = PlacePicker.getPlace(data, getActivity());
                if (!place.getAddress().toString().isEmpty()) {
                    String toastMsg = String.format("Place: %s", place.getAddress());
                    Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
                    ed_address.setText(place.getAddress());
                } else {
                    Toast.makeText(getActivity(), "Please pick your address !", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        hideKeyboard();
    }

    void send_help() {

        final String str_cust_name = ed_cust_name.getText().toString();
        final String str_job_name = ed_job_name.getText().toString();
        final String str_add = ed_address.getText().toString();
        LatLng latLng = getLocationFromAddress(getActivity(), str_add);
        final String str_po = ed_po.getText().toString();
        final String str_store = ed_store.getText().toString();
        final String str_amt = ed_amount.getText().toString();
        final String str_contact = ed_contact_number.getText().toString();
        final String str_desc = ed_job_desc.getText().toString();

        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonReq_manage_job = new StringRequest(Request.Method.GET, url + "driver_id=" + uid + "&cus_name=" + str_cust_name.replace(" ", "%20")
                    + "&job_name=" + str_job_name.replace(" ", "%20")
                    + "&address=" + str_add.replace(" ", "%20")
                    + "&lat=" + String.valueOf(latLng.latitude)
                    + "&lng=" + String.valueOf(latLng.longitude)
                    + "&date=" + parseDateToddMMyyyy(txt_from_date.getText().toString())
                    + "&from_time=" + txt_from_time.getText().toString()
                    + "&to_time=" + txt_to_time.getText().toString()
                    + "&amount=" + str_amt.replace(" ", "%20")
                    + "&po=" + str_po.replace(" ", "%20")
                    + "&store=" + str_store.replace(" ", "%20")
                    + "&category_id=" + String.valueOf(category)
                    + "&job_label=" + job_label.trim()
                    + "&desc=" + str_desc.replace(" ", "%20")
                    + "&invoice_type=" + str_invoice.trim()
                    + "&conatct=" + str_contact.replace(" ", "%20"), new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.e(TAG, response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Job Added !", Toast.LENGTH_LONG).show();
                                Intent intent_smith = new Intent(getActivity(), Main_Activity_Nevigation.class);
                                intent_smith.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent_smith);
                                getActivity().overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
//                          overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                                getActivity().finish();
                                break;
                            default:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideKeyboard();
                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonReq_manage_job);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd MMM yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed_amount.getWindowToken(), 0);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manage_job_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = manage_job_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

}
