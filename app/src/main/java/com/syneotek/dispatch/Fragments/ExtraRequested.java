package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.ExtraAdapter;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Syneotek on 3/10/2016.
 */
public class ExtraRequested extends Fragment {


    private static final String TAG = ExtraRequested.class.getSimpleName();

    private static final String url = Const.HOSTNAME + "extrarequest.php?";
    private ProgressDialog pDialog;
    private List<app_name_items> appNameItemse_list = new ArrayList<app_name_items>();
    private ListView listView;
    private TextView noData;
    private ExtraAdapter adapter;
    private TextView txt_current_date;
    public static String cur_date = "";
    public static String uid = "";
    private ImageView img_previous, img_next;
    public static int dateCount = 0;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_extra_requested, container, false);
        listView = (ListView) v.findViewById(R.id.list);
        noData = (TextView) v.findViewById(R.id.noData);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        adapter = new ExtraAdapter(getActivity(), appNameItemse_list);
        listView.setAdapter(adapter);


//        getListData(cur_date);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                app_name_items item;
                item = appNameItemse_list.get(position);

                if (!item.getStatus().equalsIgnoreCase("ne")) {
                    Intent intent_smith = new Intent(getActivity(), Survay_Details.class);
                    intent_smith.putExtra("Job_ID", item.getJID());
                    intent_smith.putExtra("Details", item.getDetails());
                    intent_smith.putExtra("Address", item.getAddress());
                    intent_smith.putExtra("Lat", item.getLat());
                    intent_smith.putExtra("Lang", item.getLango());
                    intent_smith.putExtra("Status", item.getStatus());
                    intent_smith.putExtra("Assigneddate", item.getADATE());
                    intent_smith.putExtra("Workinddate", item.getWorkingdate());
                    intent_smith.putExtra("Certificate", item.getCert());
                    intent_smith.putExtra("Description", item.getDesc());
                    intent_smith.putExtra("contact", item.getcontact());
                    intent_smith.putExtra("from_time", item.getfrom_time());
                    intent_smith.putExtra("to_time", item.getto_time());
                    intent_smith.putExtra("po", item.getpo());
                    intent_smith.putExtra("store", item.getstore());
                    intent_smith.putExtra("attachment", "no"/*item.getAttachment()*/);
                    intent_smith.putExtra("price", item.getPrice());

                    int a = item.getStype();
                    intent_smith.putExtra("stype", item.getStype());

                   /* intent_smith.putExtra("olat",item.getOlat());
                    intent_smith.putExtra("olng",item.getOlang());

                    intent_smith.putExtra("hlat",item.getHlat());
                    intent_smith.putExtra("hlng",item.getHlang());*/

                    intent_smith.putExtra("olat", "0");
                    intent_smith.putExtra("olng", "0");

                    intent_smith.putExtra("hlat", "0");
                    intent_smith.putExtra("hlng", "0");


                    Log.e("ITEM----->", item.getStatus() + "");
                    Log.e("ITEM----->", item.getAttachment() + "");

                    startActivity(intent_smith);
//                    progressDialog.dismiss();
                    getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                }


            }
        });

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        // getListData();

        getListData();
    }

    void getListData() {
        if (isNetworkAvailable()) {

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            appNameItemse_list.clear();
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonObjApp = new StringRequest(Request.Method.GET, url + "driver_id=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.e(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    app_name_items data = new app_name_items();
                                    data.setJID(jsonObject.getString("job_id"));
                                    data.setDetails(jsonObject.getString("detail"));
                                    data.setAddress(jsonObject.getString("address"));
                                    data.setLat(jsonObject.getString("lat"));
                                    data.setLango(jsonObject.getString("lang"));
                                    data.setStatus(jsonObject.getString("status"));
                                    data.setADATE(jsonObject.getString("assigned_date"));
                                    data.setWorkinddate(jsonObject.getString("working_date"));
                                    data.setCert(jsonObject.getString("certificate"));
                                    data.setDesc(jsonObject.getString("description"));
                                    data.setcontact(jsonObject.getString("contact"));
                                    data.setfrom_time(jsonObject.getString("from_time"));
                                    data.setto_time(jsonObject.getString("to_time"));
                                    data.setpo(jsonObject.getString("po"));
                                    data.setstore(jsonObject.getString("store"));

                                    data.setPrice(jsonObject.getString("amount"));
                                    data.setExtaCost(jsonObject.getString("extracost"));

                                    data.setComment(jsonObject.getString("comment"));
                                    data.setimage_attachment(jsonObject.getString("image_attachment"));


                                   /* data.setOlat(jsonObject.getString("olat"));
                                    data.setOlat(jsonObject.getString("olng"));

                                    data.setHlat(jsonObject.getString("hlat"));
                                    data.setHlang(jsonObject.getString("hlng"));*/


                                    Log.d("ATTACHMENT", jsonObject.getString("image_attachment"));
                                    appNameItemse_list.add(data);

                                }
                                adapter.notifyDataSetChanged();
                                listView.setVisibility(ListView.VISIBLE);
                                noData.setVisibility(TextView.GONE);
                                progressDialog.dismiss();
                                break;
                            default:
                                noData.setVisibility(TextView.VISIBLE);
                                listView.setVisibility(ListView.GONE);
                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                        noData.setVisibility(TextView.VISIBLE);
                        listView.setVisibility(ListView.GONE);
                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    noData.setVisibility(TextView.VISIBLE);
                    listView.setVisibility(ListView.GONE);
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjApp);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager app_name_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = app_name_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
