package com.syneotek.dispatch.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.CustomListAdapter;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Calender_Frag extends Fragment {
    private static final String TAG = App_Name_Frag1.class.getSimpleName();

    private static final String url = Const.HOSTNAME + "wsgetjob.php?";
    private List<app_name_items> appNameItemse_list = new ArrayList<app_name_items>();
    private ListView listView;
    private CustomListAdapter adapter;
    private CalendarView calendarView;
    public static String cur_date = "";
    public static String uid = "";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.calender, container, false);

        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        cur_date = df.format(new Date());

        calendarView = (CalendarView) v.findViewById(R.id.calendarView);
        calendarView.setDate(System.currentTimeMillis());

        calendarView.setShowWeekNumber(false);
        calendarView.setFirstDayOfWeek(2);
        calendarView.setSelectedWeekBackgroundColor(Color.TRANSPARENT);
        calendarView.setUnfocusedMonthDateColor(Color.GRAY);
        calendarView.setWeekSeparatorLineColor(Color.TRANSPARENT);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                cur_date = "" + year + "-" + (month + 1) + "-" + dayOfMonth;
//                Toast.makeText(getActivity(),cur_date,Toast.LENGTH_SHORT).show();
                getListData(cur_date);
            }
        });

        listView = (ListView) v.findViewById(R.id.list);
        adapter = new CustomListAdapter(getActivity(), appNameItemse_list);
        listView.setAdapter(adapter);
        getListData(cur_date);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setMessage("Loading...");
//                progressDialog.show();
                app_name_items item = new app_name_items();
                item = appNameItemse_list.get(position);

                if (!item.getStatus().equalsIgnoreCase("ne")) {
                    Intent intent_smith = new Intent(getActivity(), Survay_Details.class);
                    intent_smith.putExtra("Job_ID", item.getJID());
                    intent_smith.putExtra("Details", item.getDetails());
                    intent_smith.putExtra("Address", item.getAddress());
                    intent_smith.putExtra("Lat", item.getLat());
                    intent_smith.putExtra("Lang", item.getLango());
                    intent_smith.putExtra("Status", item.getStatus());
                    intent_smith.putExtra("Assigneddate", item.getADATE());
                    intent_smith.putExtra("Workinddate", item.getWorkingdate());
                    intent_smith.putExtra("Certificate", item.getCert());
                    intent_smith.putExtra("Description", item.getDesc());
                    intent_smith.putExtra("contact", item.getcontact());
                    intent_smith.putExtra("from_time", item.getfrom_time());
                    intent_smith.putExtra("to_time", item.getto_time());
                    intent_smith.putExtra("po", item.getpo());
                    intent_smith.putExtra("store", item.getstore());
                    intent_smith.putExtra("attachment", item.getAttachment());
                    intent_smith.putExtra("price", item.getPrice());
                    intent_smith.putExtra("stype", item.getStype());

                    intent_smith.putExtra("olat", item.getOlat());
                    intent_smith.putExtra("olng", item.getOlng());

                    intent_smith.putExtra("hlat", item.getHlat());
                    intent_smith.putExtra("hlng", item.getHlng());


                    // intent_smith.putExtra("stype", item.getStype());

                    startActivity(intent_smith);
                    getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
//                    progressDialog.dismiss();
                }
            }
        });
        return v;
    }

    void getListData(String current_date) {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            appNameItemse_list.clear();
            final StringRequest jsonObjReq_cal = new StringRequest(Request.Method.GET, url + "driver_id=" + uid + "&date=" + current_date, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONArray("info");
                        for (int l = 0; l < jsonArray.length(); l++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(l);

                            app_name_items data = new app_name_items();
                            data.setJID(jsonObject.getString("job_id"));
                            data.setDetails(jsonObject.getString("detail"));
                            data.setAddress(jsonObject.getString("address"));
                            data.setLat(jsonObject.getString("lat"));
                            data.setLango(jsonObject.getString("lang"));
                            data.setStatus(jsonObject.getString("status"));
                            data.setADATE(jsonObject.getString("assigned_date"));
                            data.setWorkinddate(jsonObject.getString("working_date"));
                            data.setCert(jsonObject.getString("certificate"));
                            data.setDesc(jsonObject.getString("description"));
                            data.setcontact(jsonObject.getString("contact"));
                            data.setfrom_time(jsonObject.getString("from_time"));
                            data.setto_time(jsonObject.getString("to_time"));
                            data.setpo(jsonObject.getString("po"));
                            data.setstore(jsonObject.getString("store"));
                            data.setAttachment(jsonObject.getString("file_name"));
                            data.setPrice(jsonObject.getString("amount"));
                            data.setStype(jsonObject.getInt("stype"));

                            data.setOlat(jsonObject.getString("olat"));
                            data.setOlng(jsonObject.getString("olng"));

                            data.setHlat(jsonObject.getString("hlat"));
                            data.setHlng(jsonObject.getString("hlng"));

                            Log.d("225 Item Data", data.toString());
                            appNameItemse_list.add(data);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                    adapter.notifyDataSetChanged();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonObjReq_cal);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public static Calender_Frag newInstance(String text) {
        Calender_Frag f = new Calender_Frag();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager cal_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cal_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}



