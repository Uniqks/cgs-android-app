package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.Help_Msg_Adapter;
import com.syneotek.dispatch.model.HelpMsg;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by Syneotek on 13/04/2016.
 */
public class Help_Fragment extends Fragment {

    private static final String TAG = "Help_Fragment";
    private static final String url = Const.HOSTNAME + "wshelp.php?";
    private static final String url_help_list = Const.HOSTNAME + "wsgethelp.php?";
    public static String uid = "";
    private ListView listView_help;
    private List<HelpMsg> list_msg = new ArrayList<HelpMsg>();
    private Help_Msg_Adapter adapter;
    TextView txt_submit_help, txt_cancle_help;
    EditText ed_help;
    private Timer timer;
    ProgressDialog progressDialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.help_fragment, container, false);
        listView_help = (ListView) v.findViewById(R.id.list_help_hf);
        adapter = new Help_Msg_Adapter(getActivity(), list_msg);
        listView_help.setAdapter(adapter);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");

        ed_help = (EditText) v.findViewById(R.id.ed_help_hf);

        txt_submit_help = (TextView) v.findViewById(R.id.txt_submit_help_hf);
        txt_submit_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_help.getText().toString().trim().isEmpty()) {
                    ed_help.setError("Please enter some text !");
                } else {
                    send_help(ed_help.getText().toString());
                }
            }
        });

        txt_cancle_help = (TextView) v.findViewById(R.id.txt_cancle_help_hf);
        txt_cancle_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                ed_help.getText().clear();
              //  finish();
//                overridePendingTransition(R.anim.slide_stay, R.anim.slide_down);
            }
        });

        ed_help.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ed_help.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ed_help.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ed_help.setError(null);
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.e("HelpFragment", "Refresh");
                loadHelpMsgList();
            }
        }, TimeUnit.SECONDS.toMillis(1), TimeUnit.SECONDS.toMillis(10));

    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timer = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        hideKeyboard();
//        loadHelpMsgList();
    }

    void send_help(String msg) {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
//            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Loading...");
            if (!progressDialog.isShowing()) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.show();
                    }
                });
            }
            final StringRequest jsonReq_help_hf = new StringRequest(Request.Method.GET, url + "sender_id=" + uid + "&message=" + msg.replace(" ", "%20"), new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Help message send !", Toast.LENGTH_LONG).show();
                                ed_help.getText().clear();
                                loadHelpMsgList();
                                break;
                            default:
                                hideKeyboard();
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideKeyboard();
                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonReq_help_hf);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void loadHelpMsgList() {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
//            if (!progressDialog.isShowing()) {
//                getActivity().runOnUiThread(new Runnable() {
//                    public void run() {
//                        progressDialog.show();
//                    }
//                });
//            }
            final StringRequest jsonObjMsgList_hf = new StringRequest(Request.Method.GET, url_help_list + "driver_id=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                list_msg.clear();
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    HelpMsg data = new HelpMsg();
                                    data.sender_id = jsonObject.getString("sender_id");
                                    data.sender_type = jsonObject.getString("sender_type");
                                    data.receiever_id = jsonObject.getString("receiever_id");
                                    data.receiever_type = jsonObject.getString("receiver_type");
                                    data.help_message = jsonObject.getString("message");
                                    data.date = jsonObject.getString("date");
                                    list_msg.add(data);

                                }
                                adapter.notifyDataSetChanged();
//                                progressDialog.dismiss();
                                break;
                            default:
//                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
//                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error " + error.toString(), Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjMsgList_hf);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed_help.getWindowToken(), 0);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager help_cm_hf = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = help_cm_hf
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
