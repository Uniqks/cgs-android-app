package com.syneotek.dispatch.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.RatingListAdapter;
import com.syneotek.dispatch.dispatch.activity.Login_Activity;
import com.syneotek.dispatch.model.RatingListItem;
import com.syneotek.dispatch.utils.Const;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingReviewFragment extends Fragment {

    private static final String url = Const.HOSTNAME + "rating.php?";
    ListView ratingList;
    public static String uid = "";
    List<RatingListItem> ratingItemList = new ArrayList<>();
    String TAG = "RatingAndReview";
    RatingListAdapter adapter;
    TextView noData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rating_review, container, false);

        ratingList = (ListView) view.findViewById(R.id.ratingList);
        noData = (TextView) view.findViewById(R.id.noData);

        adapter = new RatingListAdapter(getActivity(), ratingItemList);
        ratingList.setAdapter(adapter);
        getListData();
        return view;
    }


    void getListData() {
        if (isNetworkAvailable()) {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Login_Activity.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            ratingItemList.clear();
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonObjApp = new StringRequest(Request.Method.GET, url + "driver_id=" + uid, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String dataStatus = object.getString("status");
                        switch (Integer.parseInt(dataStatus)) {
                            case 1:
                                JSONArray jsonArray = object.getJSONArray("info");
                                for (int l = 0; l < jsonArray.length(); l++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(l);

                                    RatingListItem data = new RatingListItem();
                                    data.setJobName(jsonObject.getString("detail"));
                                    data.setInstallationRating(jsonObject.getString("Rating"));
                                    data.setCleanRating(jsonObject.getString("Rating1"));
                                    data.setComment(jsonObject.getString("ratingcommet"));
                                    data.setJobExplain(jsonObject.getString("Rating2"));

                                    ratingItemList.add(data);

                                }
                                adapter.notifyDataSetChanged();
                                ratingList.setVisibility(ListView.VISIBLE);
                                noData.setVisibility(TextView.GONE);
                                progressDialog.dismiss();
                                break;
                            default:
                                noData.setVisibility(TextView.VISIBLE);
                                ratingList.setVisibility(ListView.GONE);
                                progressDialog.dismiss();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                        noData.setVisibility(TextView.VISIBLE);
                        ratingList.setVisibility(ListView.GONE);
                        progressDialog.dismiss();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                    noData.setVisibility(TextView.VISIBLE);
                    ratingList.setVisibility(ListView.GONE);
                    progressDialog.dismiss();
                }
            });

            requestQueue.add(jsonObjApp);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager app_name_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = app_name_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
