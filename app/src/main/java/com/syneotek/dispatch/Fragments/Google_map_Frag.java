package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.dispatch.survey_details.GPSTracker;
import com.syneotek.dispatch.dispatch.survey_details.Survay_Details;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Syneotek on 3/10/2016.
 */
public class Google_map_Frag extends android.support.v4.app.Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    static final LatLng map_point = new LatLng(17.6599188, 75.90639060000001);
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    private SupportMapFragment mSupportMapFragment;
    //private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    public static LatLng my_loc;
    app_name_items items = new app_name_items();
    public static String job_id = "", details = "", address = "", lat = "", lango = "",
            com_status = "", ass_date = "", workingDate = "";
    private static final String TAG = Google_map_Frag.class.getSimpleName();
    private static final String url = Const.HOSTNAME + "wsgetjob.php?";
    public static ArrayList<app_name_items> appNameItemse_list = new ArrayList<app_name_items>();
    public static String uid = "";
    static final LatLng TutorialsPoint = new LatLng(21, 57);
//    private GoogleMap googleMap;


    GPSTracker gps;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.google_map, container, false);

//        if (isNetworkAvailable())
//        {
//            getListData(App_Name_Frag1.cur_date);
//            //if (appNameItemse_list.size() > 0)
//              // setUpMap();
//        }
//        else
//        {
//            Toast.makeText(getActivity(), "Check Your Internet Connection !", Toast.LENGTH_SHORT).show();
//        }

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

        try {

            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

            mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapwhere);
            if (mSupportMapFragment == null) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mSupportMapFragment = SupportMapFragment.newInstance();
                fragmentTransaction.replace(R.id.mapwhere, mSupportMapFragment).commit();
            }

            setMap_Data();
            getListData(sharedpreferences.getString("cur_date", App_Name_Frag1.cur_date));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    void getListData(String current_date) {
        if (isNetworkAvailable()) {

            //   setMap_Data();
            Log.e("current_date :", current_date);
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final StringRequest jsonObjApp = new StringRequest(Request.Method.GET, url + "driver_id=" + uid + "&date=" + current_date, new Response.Listener<String>() {
                public void onResponse(String response) {
                    Log.d(TAG, response);
//                    Toast.makeText(getContext(),response,Toast.LENGTH_SHORT).show();

                    progressDialog.dismiss();
                    try {
                        appNameItemse_list.clear();
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONArray("info");
                        for (int l = 0; l < jsonArray.length(); l++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(l);

                            app_name_items data = new app_name_items();
                            data.setJID(jsonObject.getString("job_id"));
                            data.setDetails(jsonObject.getString("detail"));
                            data.setAddress(jsonObject.getString("address"));
                            data.setLat(jsonObject.getString("lat"));
                            data.setLango(jsonObject.getString("lang"));
                            data.setStatus(jsonObject.getString("status"));
                            data.setADATE(jsonObject.getString("assigned_date"));
                            data.setWorkinddate(jsonObject.getString("working_date"));
                            data.setCert(jsonObject.getString("certificate"));
                            data.setDesc(jsonObject.getString("description"));
                            data.setcontact(jsonObject.getString("contact"));
                            data.setfrom_time(jsonObject.getString("from_time"));
                            data.setto_time(jsonObject.getString("to_time"));
                            data.setpo(jsonObject.getString("po"));
                            data.setstore(jsonObject.getString("store"));
                            data.setAttachment(jsonObject.getString("file_name"));
                            data.setPrice(jsonObject.getString("amount"));
                            data.setStype(jsonObject.getInt("stype"));

                            data.setOlat(jsonObject.getString("olat"));
                            data.setOlng(jsonObject.getString("olng"));

                            data.setHlat(jsonObject.getString("hlat"));
                            data.setHlng(jsonObject.getString("hlng"));

                            Log.d("O Test : ", data.toString());

                            appNameItemse_list.add(data);
                        }
                        Log.e("appNameItemse_list :", "" + appNameItemse_list.size());
                        setMap_Data();

                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
//                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonObjApp);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void setMap_Data() {
        if (mSupportMapFragment != null) {

            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {


                   /* googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    googleMap.setMyLocationEnabled(true);*/


                    gps = new GPSTracker(getActivity());
                    if (gps.canGetLocation()) {
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        my_loc = new LatLng(latitude, longitude);

                        Log.d("My Location", "" + latitude + " , " + longitude);


                        Marker TP = googleMap.addMarker(new MarkerOptions()
                                .position(my_loc).title("My Location")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                                .snippet(getCompleteAddressString(latitude, longitude)).visible(true)
                        );

                    }

                    if (appNameItemse_list.size() == 0) {
                        CameraPosition cameraPosition = new CameraPosition.Builder().
                                target(my_loc).zoom(4.0f).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.moveCamera(cameraUpdate);
                    }

                    googleMap.getUiSettings().setAllGesturesEnabled(true);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    Log.e("onMapReady :", "");

//                            if (appNameItemse_list.size() > 0) {
                    Log.e("Map", "Size=" + appNameItemse_list.size());
                    for (int i = 0; i < appNameItemse_list.size(); i++) {
                        items = appNameItemse_list.get(i);
                        Log.e("Map", "Size=" + items.toString());
                        float marker_color = 0;
                        if (items.getStatus().equalsIgnoreCase("c")) {
                            marker_color = BitmapDescriptorFactory.HUE_GREEN;
                        } else if (items.getStatus().equalsIgnoreCase("p")) {
                            marker_color = BitmapDescriptorFactory.HUE_YELLOW;
                        } else if (items.getStatus().equalsIgnoreCase("ne")) {
                            marker_color = BitmapDescriptorFactory.HUE_RED;
                        } else if (items.getStatus().equalsIgnoreCase("e")) {
                            marker_color = BitmapDescriptorFactory.HUE_ORANGE;
                        } else if (items.getStatus().equalsIgnoreCase("a")) {
                            marker_color = BitmapDescriptorFactory.HUE_AZURE;
                        }
                        if (!items.getLat().equals("") || !items.getLango().equals("")) {
                            googleMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.valueOf(items.getLat()), Double.valueOf(items.getLango()))).title(items.getDetails())
                                    .icon(BitmapDescriptorFactory.defaultMarker(marker_color))
                                    //  .animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(items.getLat(), items.getLango()), 8.0f))
                                    .snippet(items.getAddress()));

                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.valueOf(items.getLat()), Double.valueOf(items.getLango()))).zoom(4.0f).build();
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                            googleMap.moveCamera(cameraUpdate);


                            //  googleMap.setMyLocationEnabled(true);

                        }

                    }
                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            for (int i = 0; i < appNameItemse_list.size(); i++) {
                                items = appNameItemse_list.get(i);
                                if (!items.getStatus().equalsIgnoreCase("ne")) {
                                    if (marker.getSnippet().contains(items.getAddress())) {
                                        Intent intent_smith = new Intent(getContext(), Survay_Details.class);
                                        intent_smith.putExtra("Job_ID", items.getJID());
                                        intent_smith.putExtra("Details", items.getDetails());
                                        intent_smith.putExtra("Address", items.getAddress());
                                        intent_smith.putExtra("Lat", items.getLat());
                                        intent_smith.putExtra("Lang", items.getLango());
                                        intent_smith.putExtra("Status", items.getStatus());
                                        intent_smith.putExtra("Assigneddate", items.getADATE());
                                        intent_smith.putExtra("Workinddate", items.getWorkingdate());
                                        intent_smith.putExtra("Certificate", items.getCert());
                                        intent_smith.putExtra("Description", items.getDesc());
                                        intent_smith.putExtra("contact", items.getcontact());
                                        intent_smith.putExtra("from_time", items.getfrom_time());
                                        intent_smith.putExtra("to_time", items.getto_time());
                                        intent_smith.putExtra("po", items.getpo());
                                        intent_smith.putExtra("store", items.getstore());
                                        intent_smith.putExtra("attachment", items.getAttachment());
                                        intent_smith.putExtra("price", items.getPrice());
                                        intent_smith.putExtra("stype", items.getStype());


                                        intent_smith.putExtra("olat", items.getOlat());
                                        intent_smith.putExtra("olng", items.getOlng());

                                        intent_smith.putExtra("hlat", items.getHlat());
                                        intent_smith.putExtra("hlng", items.getHlng());

                                        startActivity(intent_smith);
                                        getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_stay);
                                    }
                                }
                            }
                        }
                    });
//                            }
                }
            });
        }
    }

    public static Google_map_Frag newInstance(String text) {
        Google_map_Frag f = new Google_map_Frag();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager map_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = map_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                StringBuilder strReturnedAddress = new StringBuilder("");
                strReturnedAddress.append(addresses.get(0).getAddressLine(0)).append(",");
                strReturnedAddress.append(addresses.get(0).getLocality()).append(",");
                strReturnedAddress.append(addresses.get(0).getAdminArea()).append(",");
                strReturnedAddress.append(addresses.get(0).getCountryName());
//                strReturnedAddress.append(addresses.get(0).getPostalCode()).append(",");
//                strReturnedAddress.append(addresses.get(0).getFeatureName());
                strAdd = strReturnedAddress.toString();
                Log.w("MyCurrentloctionaddress", "" + strReturnedAddress.toString());
            } else {
                Log.w("MyCurrentloctionaddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("MyCurrentloctionaddress", "Canont get Address!");
        }
        return strAdd;
    }
}


//package com.syneotek.dispatch.Fragments;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.syneotek.dispatch.R;
//
///**
// * Created by Syneotek on 3/10/2016.
// */
//public class Google_map_Frag extends Fragment
//{
//    static final LatLng map_point = new LatLng(17.6599188,75.90639060000001 );
//    private GoogleMap googleMap;
//
//
//    public static String job_id = "",details = "", address = "",lat="",lango="",com_status = "",ass_date="", workingDate = "" ;
//
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
//    {
//
//        View v = inflater.inflate(R.layout.google_map, container, false);
//
//        Intent intent = getActivity().getIntent();
//        job_id= intent.getStringExtra("Job_ID");
//        details = intent.getStringExtra("Details");
//        address = intent.getStringExtra("Address");
//        lat= intent.getStringExtra("Lat");
//        lango= intent.getStringExtra("Lang");
//        ass_date= intent.getStringExtra("Assigneddate");
//        workingDate = intent.getStringExtra("Workinddate");
//        com_status = intent.getStringExtra("Status");
//
//        try
//        {
//            if (googleMap == null)
//            {
//                googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.google_map_one)).getMap();
//            }
//            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//            Marker TP = googleMap.addMarker(new MarkerOptions()
//                            .position(map_point).title("My Location")
//                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
//                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.google_icon))
//                            .snippet("current")
//
//            );
//
//            if (!lat.equalsIgnoreCase("") && !lango.equalsIgnoreCase("")) {
//                Marker TP_Job = googleMap.addMarker(new MarkerOptions()
//                                .position(new LatLng(Double.valueOf(lat), Double.valueOf(lango))).title("Job Location")
//                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
//                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.google_icon))
//                                .snippet("job")
//                );
//            }
//
////            CameraPosition cameraPos = new CameraPosition.Builder()
////                    .target(new LatLng(Double.parseDouble("17.6599188"), Double.parseDouble("75.90639060000001")))
////                    .zoom(10).bearing(0).tilt(0).build();
////            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos), null);
//
//            //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(map_point,40));
//            // googleMap.animateCamera(CameraUpdateFactory.zoomTo(10),2000,null);
//
//
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    return v;
//
//}
//    public static Google_map_Frag newInstance(String text)
//    {
//        Google_map_Frag f = new Google_map_Frag();
//        Bundle b = new Bundle();
//        b.putString("msg", text);
//
//        f.setArguments(b);
//
//        return f;
//    }
//
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK)
//        {
//
//            // getActivity.finish();
//
////            Intent intent=new Intent(getActivity(),Main_Activity_Nevigation.class);
////            startActivity(intent);
////            getActivity().finish();
//
////            FragmentManager fm = getFragmentManager();
////            FragmentTransaction fragmentTransaction = fm.beginTransaction();
////
////            fragmentTransaction.replace(R.id.main_screen_framelayout, new App_Name_Frag1());
////            fragmentTransaction.addToBackStack(null);
////
////            fragmentTransaction.commit();
////            getActivity().finish();
////            return true;
//        }
//
//        return getActivity().onKeyDown(keyCode, event);
//    }
//
//
//}
