package com.syneotek.dispatch.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.syneotek.dispatch.R;
import com.syneotek.dispatch.adapter.MyPublicButtAdapter;
import com.syneotek.dispatch.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Syneotek1 on 1/10/2017.
 */

public class ViewGalleryImages extends Fragment {

    private ArrayList<String> mDataSet = new ArrayList<String>();
    MyPublicButtAdapter adapter;
    RecyclerView recyclerView;
    TextView txtNoData;

    private static final String getGalleryIMgUrl = Const.HOSTNAME + "wsgetimagelist.php?";

    public static String uid = "";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_view_uploaded_images, container, false);

        txtNoData = (TextView) v.findViewById(R.id.txtNoData);
        txtNoData.setVisibility(View.GONE);

        recyclerView = (RecyclerView) v.findViewById(R.id.buttListView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new MyPublicButtAdapter(getActivity(), mDataSet);
        recyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        subInstallerCall();
    }

    void subInstallerCall() {
        if (isNetworkAvailable()) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            uid = sharedpreferences.getString("uid", "0");
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            StringRequest jsonReq_getImg = new StringRequest(Request.Method.GET, getGalleryIMgUrl
                    + "driver_id=" + uid
                    , new Response.Listener<String>() {
                public void onResponse(String response) {

                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        String jsonStatus = object.getString("status");

                        switch (Integer.parseInt(jsonStatus)) {
                            case 1:
                                JSONObject info = object.getJSONObject("info");
                                String images = info.getString("image");

                                if (images != null) {
                                    String[] splitImage = images.split(",");
                                    mDataSet = new ArrayList<>(Arrays.asList(splitImage));
                                }

                                adapter = new MyPublicButtAdapter(getActivity(), mDataSet);
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                break;
                            default:
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Error while loading data", Toast.LENGTH_SHORT).show();
                }
            });

            requestQueue.add(jsonReq_getImg);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager view_gal_cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = view_gal_cm
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static ViewGalleryImages newInstance(String text) {
        ViewGalleryImages f = new ViewGalleryImages();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
